#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 10:50:13 2021

@author: des
"""

from __future__ import annotations
import sys
import os
import csv
import math
from typing import List, Tuple, Dict, Any, Type, Callable, Optional
from types import SimpleNamespace
from functools import partial
import re
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import Qt
import dlib
import numpy as np
import cv2 as cv
import face_recognition
from collections import Counter
from datetime import datetime
import traceback
import argparse
from pprint import pformat
import json
from skimage.measure import label

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib import pyplot as plt
import matplotlib
from matplotlib.axes import Axes

"""
custom qt and ui thingies
"""

class IntListValidator(QtGui.QValidator):
    def __init__(self, parent=None):
        super().__init__(parent)
    def validate(self, s, pos):
        if re.match('^[0-9 ,]*$', s):
            result = QtGui.QValidator.Acceptable
        else:
            result = QtGui.QValidator.Invalid
        return (result, s, pos)
    def fixup(self, s):
        return re.sub('[^0-9 ,]', '', s)

class ImageFrame(QtWidgets.QLabel):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._unscaled_pixmap = self.pixmap()
        # TODO arbitrary defaults
        self.width = 300
        self.height = 300
    def setPixmap(self, pixmap):
        self._unscaled_pixmap = pixmap
        self.update()
    def resizeEvent(self, event: QtGui.ResizeEvent):
        size = event.size()
        self.width = size.width()
        self.height = size.height()
        self.update()
    def update(self):
        if self._unscaled_pixmap is not None:
            super().setPixmap(self._unscaled_pixmap.scaled(
                self.width, self.height, QtCore.Qt.KeepAspectRatio))

class OpenImageSelectChannelWindow(QtWidgets.QDialog):
    def __init__(self, args: SimpleNamespace, parent=None):
        super().__init__(parent)

        self.setWindowModality(Qt.ApplicationModal)

        self.buttons = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)
        layout = QtWidgets.QGridLayout()
        self.args = args
        self.picker = ChannelPicker('', 'channel', 3)
        self.picker.connect(self.args, None)
        self.keep = BoolCheckBox('Load colorful copy', 'keep', False)
        self.keep.connect(self.args, None)
        self.hello = BoolCheckBox('Create a Hello world!', 'hello', False)
        self.hello.connect(self.args, None)
        layout.addWidget(self.picker.widget, 0, 0, 1, 2)
        layout.addWidget(self.keep.widget, 1, 0, 1, 1)
        layout.addWidget(self.hello.widget, 1, 1, 1, 1)
        layout.addWidget(self.buttons, 2, 0, 1, 2)
        self.setLayout(layout)
    def preprocess_image(self, image: np.array) -> Tuple[str, np.array]:
        if not self.args.keep:
            return ('gray', processing_action_channel(image, self.args))
        else:
            return ('rgb', image)
    def generate_procedure(self, control: AppControl) -> None:
        index = 0;
        if self.args.keep:
            if self.args.hello:
                index = self._add_process(index, control, '-- comment', ['Operations on original image'])
                index = self._add_process(index, control, '-- Choose function --')
                index = self._add_process(index, control, '-- comment', ['Store original image'])
            index = self._add_process(index, control, '-- store', ['IMAGE'])
            index = self._add_process(index, control, 'channel', [self.args.channel])
        if self.args.hello:
            index = self._add_process(index, control, '-- comment', ['Operations on grayscale image here'])
            index = self._add_process(index, control, 'equalize')
            index = self._add_process(index, control, 'threshold')
            if self.args.keep:
                index = self._add_process(index, control, '-- comment', ['Example of displaying binary results'])
                index = self._add_process(index, control, 'binary overlay')
        control.update()

    def _add_process(self, index: int, control: AppControl, process_name: str, args=None) -> int:
        # TODO clean up this .................... thing
        control.add_new(index - 1, NoopProcess(control)) # TODO fix
        control.set_process(index, process_name)
        if args is not None:
            control.get_process(index).load(args)
        return index + 1;

class NewProcessWindow(QtWidgets.QDialog):
    def __init__(self, args: SimpleNamespace, ctrl: AppControl, parent=None):
        super().__init__(parent)

        self.args = args
        self.ctrl = ctrl

        self.setWindowModality(Qt.ApplicationModal)
        self.setFixedSize(480, 360)

        self.buttons = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel,
            Qt.Horizontal, self)

        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        layout = QtWidgets.QGridLayout()

        # def _create_widget(self):
        #     widget = QtWidgets.QLineEdit()
        #     widget.setText(self.default)
        #     widget.textChanged.connect(self._store)
        #     self.widget = widget
        # def _get_new_value(self):
        #     return self.widget.text()
        self.search = QtWidgets.QLineEdit(self)
        self.search.textChanged.connect(self._filter)
        self.tree = QtWidgets.QTreeWidget(self)
        self.tree.itemDoubleClicked.connect(self.accept)
        layout.addWidget(self.search, 0, 0, 1, 1)
        layout.addWidget(self.tree, 1, 0, 1, 1)
        layout.addWidget(self.buttons, 2, 0, 1, 1)
        self.setLayout(layout)
        self.search.setFocus()
        self._filter()
    def _filter(self):
        t = self.search.text().lower()
        filtered_process_tree = {
            k_outer: {k: v for k, v in v_outer.items() if t in k.lower() or t in k_outer.lower()}
            for k_outer, v_outer in process_tree.items()
        }
        self.tree.clear()
        self.tree.setColumnCount(2)
        self.tree.setColumnWidth(0, 300)
        self.tree.setHeaderLabels(['Process', 'Image type'])
        flag_select = True
        flag_select_direct = True
        to_select = None
        for category, processes_ in filtered_process_tree.items():
            category_item = QtWidgets.QTreeWidgetItem(self.tree)
            category_item.setText(0, category)
            # category_item.setDisabled(True)
            category_item.setFlags(category_item.flags() ^ Qt.ItemIsSelectable)
            category_item.setExpanded(True)
            for name, process_ctor in processes_.items():
                process = process_ctor(self.ctrl)
                process_item = QtWidgets.QTreeWidgetItem()
                # process_item.setDisabled(False)
                process_item.setText(0, name)
                if process.type_constraints() is not None:
                    type_text = '%s -> %s' % tuple(v or 'any' for v in process.type_constraints())
                    process_item.setText(1, type_text)
                category_item.addChild(process_item)
                if flag_select:
                    to_select = process_item
                    flag_select = False
                if flag_select_direct and t in name:
                    to_select = process_item
                    flag_select_direct = False
        if to_select is not None:
            to_select.setSelected(True)
            self.tree.setCurrentItem(to_select)
    def accept(self, *args):
        txt = self.tree.currentItem().text(0)
        if txt in processes:
            self.args.key = txt
            super().accept()

class VariableViewWindow(QtWidgets.QDialog):
    def __init__(self, ctrl: AppControl, parent=None):
        super().__init__(parent)

        self.ctrl = ctrl

        self.setWindowModality(Qt.ApplicationModal)
        self.setFixedSize(480, 360)

        self.buttons = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok,
            Qt.Horizontal, self)

        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        layout = QtWidgets.QGridLayout()

        self.search = QtWidgets.QLineEdit(self)
        self.search.textChanged.connect(self._filter)
        self.tree = QtWidgets.QTreeWidget(self)
        self.tree.itemDoubleClicked.connect(self.accept)
        self.delete = QtWidgets.QPushButton('Delete')
        self.delete.clicked.connect(self._delete)
        layout.addWidget(self.search, 0, 0, 1, 2)
        layout.addWidget(self.tree, 1, 0, 1, 2)
        layout.addWidget(self.delete, 2, 0, 1, 1)
        layout.addWidget(self.buttons, 2, 1, 1, 1)
        self.setLayout(layout)
        self.search.setFocus()
        self._filter()
    def _filter(self):
        t = self.search.text().lower()
        aic = self.ctrl.model.ai_model_collection
        vc = self.ctrl.get_variable_container()
        object_tree = {
            'Templates': vc._template,
            'Image variables': vc._image,
            # 'Models': aic.learning_data,
        }
        filtered_object_tree = {
            k_outer: {k: v for k, v in v_outer.items() if t in k.lower() or t in k_outer.lower()}
            for k_outer, v_outer in object_tree.items()
        }
        self.tree.clear()
        self.tree.setColumnCount(2)
        self.tree.setColumnWidth(0, 300)
        self.tree.setHeaderLabels(['Name', 'Type'])
        flag_select = True
        for category, processes_ in filtered_object_tree.items():
            category_item = QtWidgets.QTreeWidgetItem(self.tree)
            category_item.setText(0, category)
            category_item.setFlags(category_item.flags() ^ Qt.ItemIsSelectable)
            category_item.setExpanded(True)
            for name, (obj, type) in processes_.items():
                object_item = QtWidgets.QTreeWidgetItem()
                object_item.setText(0, name)
                object_item.setText(1, type)
                category_item.addChild(object_item)
                object_item.setSelected(flag_select)
                flag_select = False
    def _delete(self):
        item = self.tree.currentItem()
        if item is None:
            return
        parent = item.parent()
        if parent is None:
            return
        namespace = parent.text(0)
        name = item.text(0)
        vc = self.ctrl.get_variable_container()
        if namespace == 'Templates': # TODO Try not to use these strings
            del vc._template[name]
        if namespace == 'Image variables':
            del vc._image[name]
        self._filter()

        ... # TODO


class ConfigOption(QtWidgets.QWidget):
    def __init__(self, title: str, desc: str, opts: Dict[str], opts_consumer, checked=False, parent=None):
        super().__init__(parent)
        self.opts_consumer = opts_consumer
        self.opts = opts

        layout = QtWidgets.QGridLayout()

        button = QtWidgets.QRadioButton('', self)
        if checked:
            button.setChecked(True)
        button.toggled.connect(self.update_opts)

        title_widget = QtWidgets.QLabel(title)
        desc_widget = QtWidgets.QLabel(desc)

        layout.addWidget(button, 0, 0, 2, 1)
        layout.addWidget(title_widget, 1, 0, 1, 1)
        layout.addWidget(desc_widget, 1, 1, 1, 1)

        layout.setColumnMinimumWidth(0, 30)
        layout.setColumnStretch(0, 0)
        layout.setColumnStretch(1, 1)

        self.setLayout(layout)
    def update_opts(self, *args):
        self.opts_consumer(self.opts)


class FirstSetupWindow(QtWidgets.QDialog):
    def __init__(self, args: SimpleNamespace, ctrl: AppControl, parent=None):
        super().__init__(parent)

        self._set_opts({})
        self._ctrl = ctrl

        layout = QtWidgets.QGridLayout()

        picks = [
            {
                'title': 'Default',
                'desc': 'Alert on unsaved procedure or image, refresh output autimatically\nGood for experimenting, learning, tinkering',
                'opts': {} # default is, well, default
            },
            {
                'title': 'Deep learning',
                'desc': 'Alert on unsaved procedure, refresh output manually',
                'opts': { 'alert_on_unsaved_image': False, 'auto_refresh_default': False }
            },
            {
                'title': 'Code generation',
                'desc': 'Alert on unsaved procedure, refresh output automatically, display additional information regarding code generation compatibility',
                'opts': { 'alert_on_unsaved_image': False, 'display_codegen_info': True }
            }
        ]

        layout.addWidget(QtWidgets.QLabel('Welcome to im.py! Please select your defaults:'), 0, 0, 1, 2)
        for n, i in enumerate(picks):
            layout.addWidget(ConfigOption(self, opts_consumer=self._set_opts), n // 2 + 1, n % 2, 1, 1)
        layout.addWidget(QtWidgets.QLabel('This selection can be viewed again at any time.'), len(picks) // 2 + 1, 0, 1, 2)
        show_again = QtWidgets.QCheckBox('Show on next startup', self)
        layout.addWidget(show_again, len(picks) // 2 + 1, 0, 1, 1)
        self.buttons = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

    def _set_opts(self, opts):
        self._opts = opts

    def accept(self, *args):
        opts = self._ctrl.options() # TODO implement this in app control (it'll probably pull from the model?)
        opts |= self._opts # Mutable operation
        self._ctrl.save_options() # TODO implement this as well
        self._ctrl.load_options() # TODO also this
        super().accept(*args)



class AppUi(QtWidgets.QMainWindow):
    def __init__(self, args):
        super().__init__()

        self.setWindowTitle('im.py - image processing utility')

        matplotlib.use('Agg')

        self._create_central_widget()
        self._create_general_layout()
        self._create_actions()

        self._create_menu_bar()
        self._create_chain_box()
        self._create_option_box()
        self._create_input_image()
        self._create_output_image()

        self._app_layout = None
        self.set_layout(list(app_layouts.values())[args.layout - 1])
        self.resize(1280, 720)
        if args.maximize:
            self.setWindowState(QtCore.Qt.WindowMaximized)

        self.process_index = 0

        self.app_control = None

    # init block
    def _create_central_widget(self):
        self._central_widget = QtWidgets.QWidget(self)
        self.setCentralWidget(self._central_widget)
    def _create_general_layout(self):
        self._general_layout = QtWidgets.QGridLayout()
        self._general_layout.setSpacing(10)
        self._central_widget.setLayout(self._general_layout)
    def _create_action(self, name: str, func, keybind) -> QtWidgets.QAction:
        action = QtWidgets.QAction(name, self)
        action.triggered.connect(func)
        if keybind is not None:
            action.setShortcut(keybind)
        self._central_widget.addAction(action)
        return action
    def _create_actions(self):
        # open/save file actions
        self._action_open_image = self._create_action(
            '&Open image', self._clicked_file_open_image, Qt.CTRL + Qt.Key_O)
        self._action_save_image = self._create_action(
            '&Save image', self._clicked_file_save_image, Qt.CTRL + Qt.Key_S)
        self._action_open_image_in = self._create_action(
            'Open image in place', self._clicked_file_open_image_in, Qt.CTRL + Qt.ALT + Qt.Key_O)
        self._action_save_image_as = self._create_action(
            'Save image as', self._clicked_file_save_image_as, Qt.CTRL + Qt.ALT + Qt.Key_S)
        self._action_open_procedure = self._create_action(
            'Open &procedure', self._clicked_file_open_procedure, Qt.CTRL + Qt.SHIFT + Qt.Key_O)
        self._action_save_procedure = self._create_action(
            'Save p&rocedure', self._clicked_file_save_procedure, Qt.CTRL + Qt.SHIFT + Qt.Key_S)
        self._action_open_procedure_in = self._create_action(
            'Open procedure in place', self._clicked_file_open_procedure_in, Qt.CTRL + Qt.SHIFT + Qt.ALT + Qt.Key_O)
        self._action_save_procedure_as = self._create_action(
            'Save procedure as', self._clicked_file_save_procedure_as, Qt.CTRL + Qt.SHIFT + Qt.ALT + Qt.Key_S)
        # set layout actions
        self._actions_set_layout = []
        for n, (k, i) in enumerate(app_layouts.items()):
            name = '&' + str(n+1) + ' ' + k
            action = QtWidgets.QAction(name, self)
            action.triggered.connect(partial(self.set_layout, layout=i))
            self._actions_set_layout.append(action)
        # process box actions
        self._action_add_new = self._create_action(
            'Add &new', self._clicked_add_new, Qt.CTRL + Qt.Key_N)
        self._action_delete = self._create_action(
            '&Delete', self._clicked_delete, Qt.Key_Delete)
        self._action_move_up = self._create_action(
            'Move &up', self._clicked_move_up, Qt.CTRL + Qt.Key_Up)
        self._action_move_down = self._create_action(
            'Move &down', self._clicked_move_down, Qt.CTRL + Qt.Key_Down)
        self._action_pause = self._create_action(
            '&Pause', self._clicked_pause, Qt.CTRL + Qt.Key_P)
        self._action_update = self._create_action(
            'Upd&ate', self._clicked_update, Qt.CTRL + Qt.Key_R)
        # process box context menu
        self._action_chain_box_context_menu = self._create_action(
            '', partial(self._right_clicked_chain_box_item, point=QtCore.QPoint(0, 0)), Qt.CTRL + Qt.Key_Space)
        # batch actions
        self._action_batch_process = self._create_action(
            '&Batch process images', self._clicked_batch_process, Qt.CTRL + Qt.Key_B)
        self._action_batch_process_in = self._create_action(
            'Batch process images &in place', self._clicked_batch_process_in, Qt.CTRL + Qt.ALT + Qt.Key_B)
        # model actions
        self._action_model_open_template = self._create_action(
            'Load &template', self._clicked_model_open_template, None)
        # self._action_model_open_template_set = self._create_action(
        #     'Load &template set', self._clicked_model_open_template_set, None)
        self._action_model_open_learning_set = self._create_action(
            'Load &learning set', self._clicked_model_open_learning_set, None)
        self._action_model_open_ai_model = self._create_action(
            'Load &ai model', self._clicked_model_open_ai_model, None)
        self._action_model_add_variable = self._create_action(
            'Add image &variable', self._clicked_model_add_variable, None)
        self._action_model_view_variables = self._create_action(
            'View namespace', self.dialog_variable_view, None)
    def _create_menu_bar(self):
        bar = self.menuBar()

        file_menu = QtWidgets.QMenu('&File', self)
        file_menu.addAction(self._action_open_image)
        file_menu.addAction(self._action_save_image)
        file_menu.addAction(self._action_open_image_in)
        file_menu.addAction(self._action_save_image_as)
        file_menu.addSeparator()
        file_menu.addAction(self._action_open_procedure)
        file_menu.addAction(self._action_save_procedure)
        file_menu.addAction(self._action_open_procedure_in)
        file_menu.addAction(self._action_save_procedure_as)
        bar.addMenu(file_menu)

        view_menu = QtWidgets.QMenu('&View', self)
        for i in self._actions_set_layout:
            view_menu.addAction(i)
        bar.addMenu(view_menu)

        proc_menu = QtWidgets.QMenu('&Process', self)
        proc_menu.addAction(self._action_add_new)
        proc_menu.addAction(self._action_delete)
        proc_menu.addAction(self._action_move_up)
        proc_menu.addAction(self._action_move_down)
        proc_menu.addSeparator()
        proc_menu.addAction(self._action_pause)
        proc_menu.addAction(self._action_update)
        bar.addMenu(proc_menu)

        batch_menu = QtWidgets.QMenu('&Batch', self)
        batch_menu.addAction(self._action_batch_process)
        batch_menu.addAction(self._action_batch_process_in)
        bar.addMenu(batch_menu)

        model_menu = QtWidgets.QMenu('&Model', self)
        model_menu.addAction(self._action_model_open_template)
        # model_menu.addAction(self._action_model_open_template_set)
        model_menu.addAction(self._action_model_open_learning_set)
        model_menu.addAction(self._action_model_open_ai_model)
        model_menu.addSeparator()
        model_menu.addAction(self._action_model_add_variable)
        model_menu.addAction(self._action_model_view_variables)
        bar.addMenu(model_menu)
    def _create_status_bar(self):
        bar = self.statusBar()
        vertical_line = QtWidgets.QFrame()
        vertical_line.setFrameShape(QtWidgets.QFrame.VLine)
        vertical_line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.img_savedate = None # TODO init these somewhere else
        self.src_savedate = None # And update update_text somewhere else..
        datetime.now()
        self.update_text = QtWidgets.QLabel('Last updated 42 years ago')
        bar.addPermanentWidget(self.update_text)
        bar.addPermanentWidget(vertical_line)
        bar.addPermanentWidget(QtWidgets.QLabel('haha funi status bar widget'))
        self.setStatusBar(bar)
    def _create_chain_box(self):
        self._chain_box_widget = QtWidgets.QWidget(self)
        self._chain_box_layout = QtWidgets.QHBoxLayout()
        self._chain_box_widget.setLayout(self._chain_box_layout)
        self._chain_box = QtWidgets.QListWidget()
        self._chain_box.setIconSize(QtCore.QSize(12, 12))
        self._chain_box.setContextMenuPolicy(Qt.CustomContextMenu)
        self._chain_box.customContextMenuRequested.connect(
            self._right_clicked_chain_box_item)
        self._chain_box.currentItemChanged \
            .connect(self._clicked_chain_box_item)
        self._chain_box_layout.addWidget(self._chain_box)
        self._chain_box_buttons_layout = QtWidgets.QVBoxLayout()
        self._chain_box_layout.addLayout(self._chain_box_buttons_layout)
        self._create_chain_box_buttons()
    def _create_option_box(self):
        self._option_box = QtWidgets.QStackedWidget(self._central_widget)
        # Nothing added here - functions add their own options
    def _create_input_image(self):
        self._input_image_frame = QtWidgets.QStackedWidget()
        for k, v in image_types.items():
            tab = v.create_input_tab_widget(self._input_image_frame)
            tab.currentChanged.connect(self._input_tab_changed)
        # Nothing added here - pixmap is added with set_input_image
    def _create_output_image(self):
        self._output_image_frame = QtWidgets.QStackedWidget()
        for k, v in image_types.items():
            tab = v.create_output_tab_widget(self._output_image_frame)
            tab.currentChanged.connect(self._output_tab_changed)
        # self._output_image_frame = ImageFrame()
        # self._output_image_frame.setAlignment(Qt.AlignCenter)
        # Nothing added here - pixmap is added with set_output_image
    def _create_chain_box_buttons(self):
        button = QtWidgets.QPushButton()
        button.setText('Add new')
        button.setToolTip('Create a new process after the currently selected.')
        button.clicked.connect(self._action_add_new.trigger)
        self._chain_box_buttons_layout.addWidget(button)
        button = QtWidgets.QPushButton()
        button.setText('Delete')
        button.setToolTip('Delete the currently selected process.')
        button.clicked.connect(self._action_delete.trigger)
        self._chain_box_buttons_layout.addWidget(button)
        button = QtWidgets.QPushButton()
        button.setText('Move up')
        button.setToolTip('Move the currently selected process one up.')
        button.clicked.connect(self._action_move_up.trigger)
        self._chain_box_buttons_layout.addWidget(button)
        button = QtWidgets.QPushButton()
        button.setText('Move down')
        button.setToolTip('Move the currently selected process one down.')
        button.clicked.connect(self._action_move_down.trigger)
        self._chain_box_buttons_layout.addWidget(button)
        button = QtWidgets.QPushButton()
        button.setText('Pause')
        button.setToolTip('Pause autoupdating the output image.')
        button.clicked.connect(self._action_pause.trigger)
        self._pause_button = button
        self._chain_box_buttons_layout.addWidget(button)
        button = QtWidgets.QPushButton()
        button.setText('Update')
        button.setToolTip('Update the output image manually.')
        button.clicked.connect(self._action_update.trigger)
        self._chain_box_buttons_layout.addWidget(button)

        self._chain_box_buttons_layout.addLayout(QtWidgets.QHBoxLayout(), 1)

    def link(self, ctrl: AppControl):
        self.app_control = ctrl

    def set_layout(self, layout):
        if self._app_layout is not None:
            self._cleanup_layout(self._app_layout)
        self._app_layout = layout
        self._apply_layout(self._app_layout)
    def _cleanup_layout(self, layout):
        layout.reset_column_stretches(self._general_layout)
        for n in reversed(range(self._general_layout.count())):
            self._general_layout.itemAt(n).widget().setParent(None)
    def _apply_layout(self, layout: AppLayout):
        layout.set_column_stretches(self._general_layout)
        i, j, m, n = layout.chain_box_rect
        self._general_layout.addWidget(self._chain_box_widget, i, j, m, n)
        i, j, m, n = layout.option_box_rect
        self._general_layout.addWidget(self._option_box, i, j, m, n)
        i, j, m, n = layout.input_image_rect
        self._general_layout.addWidget(self._input_image_frame, i, j, m, n)
        i, j, m, n = layout.output_image_rect
        self._general_layout.addWidget(self._output_image_frame, i, j, m, n)
    def mark_error_at(self, index: int, proc: ImageProcess, e: Exception):
        item = self._chain_box.item(index)
        # item.setIcon(QtGui.QIcon.fromTheme('dialog-error'))
        item.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MessageBoxCritical))
        item.setToolTip(str(e))
    def update(self):
        for n, i in enumerate(self._chain_box.findItems('*', QtCore.Qt.MatchWildcard)):
            i.setText(self.app_control.get_process(n).long_name())
            i.setIcon(QtGui.QIcon())
            i.setToolTip('')

    def _clicked_add_new(self):
        self.app_control.add_new_select(self._chain_box.currentIndex())
    def _clicked_delete(self):
        self.app_control.delete(self._chain_box.currentIndex())
    def _clicked_move_up(self):
        self.app_control.move_up(self._chain_box.currentIndex())
    def _clicked_move_down(self):
        self.app_control.move_down(self._chain_box.currentIndex())
    def _clicked_pause(self):
        self.app_control.pause()
        state = self._pause_button.text()
        self._pause_button.setText('Resume' if state == 'Pause' else 'Pause')
    def _clicked_update(self):
        self.app_control.update()
    def _clicked_chain_box_item(self):
        self.app_control.select_chain_box_item()
    def _clicked_function_select(self, index: QtCore.QModelIndex):
        self.app_control.set_process(
            self._chain_box.currentIndex(),
            self.get_func_select().currentText()
        )
    def _clicked_file_open_image(self):
        self.app_control.open_image()
    def _clicked_file_save_image(self):
        self.app_control.save_image()
    def _clicked_file_open_procedure(self):
        self.app_control.open_procedure()
    def _clicked_file_save_procedure(self):
        self.app_control.save_procedure()
    def _clicked_file_open_image_in(self):
        self.app_control.open_image(True)
    def _clicked_file_save_image_as(self):
        self.app_control.save_image(True)
    def _clicked_file_open_procedure_in(self):
        self.app_control.open_procedure(True)
    def _clicked_file_save_procedure_as(self):
        self.app_control.save_procedure(True)
    def _right_clicked_chain_box_item(self, point: QtCore.QPoint):
        self.create_chainbox_item_context_menu(point)
    def _clicked_batch_process(self):
        self.app_control.batch_process_images()
    def _clicked_batch_process_in(self):
        self.app_control.batch_process_images(True)
    def _input_tab_changed(self):
        self.app_control.draw_input_image()
    def _output_tab_changed(self):
        self.app_control.draw_output_image()
    def _clicked_model_open_template(self):
        self.app_control.model.ai_model_collection.open_template(self)
    # def _clicked_model_open_template_set(self):
    #     self.app_control.model.ai_model_collection.open_pattern_files(self)
    def _clicked_model_open_learning_set(self):
        self.app_control.model.ai_model_collection.open_learning_files(self)
    def _clicked_model_open_ai_model(self):
        self.app_control.model.ai_model_collection.open_recognition_model(self)
    def _clicked_model_add_variable(self):
        self.app_control.add_variable(self)

    def current_input_tab(self) -> int:
        return self._input_image_frame.currentWidget().currentIndex()
    def current_output_tab(self) -> int:
        return self._output_image_frame.currentWidget().currentIndex()
    def create_chainbox_item_context_menu(self, point: QtCore.QPoint):
        point = self._chain_box.mapToGlobal(point)
        menu = QtWidgets.QMenu(self)

        sub = QtWidgets.QMenu('&Add', self)
        action = QtWidgets.QAction('&Before', self)
        after = self._chain_box.currentIndex()
        before = after.siblingAtRow(after.row() - 1)
        action.triggered.connect(
            lambda _ : self.app_control.add_new_select(before)
        )
        sub.addAction(action)
        action = QtWidgets.QAction('&After', self)
        action.triggered.connect(
            lambda _ : self.app_control.add_new_select(after)
        )
        sub.addAction(action)
        menu.addMenu(sub)

        # sub = QtWidgets.QMenu('&Surround', self)
        # subsub = QtWidgets.QMenu('&Colorspace', self)
        # for k, v in colorspace_common_funcs.items():
        #     name = k.split('_')[1]
        #     s = name.split('2')
        #     if s[0] == 'BGR':
        #         a = list(colorspace_common_funcs.keys()).index(k)
        #         b = list(colorspace_common_funcs.keys()).index('COLOR_' + s[1] + '2' + s[0])
        #         prefix = '&' + str(a // 2 + 1) + ' '
        #         # o = 'COLOR_' + s[1] + '2' + s[0]
        #         action = QtWidgets.QAction(prefix + s[0] + '2' + s[1] + ', ' + s[1] + '2' + s[0], self)
        #         # TODO exceptionally dirty solution
        #         action.triggered.connect(
        #             lambda _, a=a, b=b :
        #                 self.app_control.add_new(after) or
        #                 self.app_control.set_process(self._chain_box.currentIndex(), 'colorspace') or
        #                 self.app_control.get_process(self._chain_box.currentIndex().row()).load([b]) or

        #                 self.app_control.add_new(before) or
        #                 self.app_control.set_process(self._chain_box.currentIndex(), 'colorspace') or
        #                 self.app_control.get_process(self._chain_box.currentIndex().row()).load([a]) or

        #                 self._chain_box.setCurrentIndex(after.siblingAtRow(after.row() + 1))
        #         )
        #         subsub.addAction(action)
        # sub.addMenu(subsub)
        # menu.addMenu(sub)

        menu.exec(point)

    def add_process(self, index: int, process: ImageProcess):
        self._chain_box.insertItem(index, process.long_name())

        option_widget = QtWidgets.QWidget(self._central_widget)
        option_widget_layout = QtWidgets.QVBoxLayout()
        option_widget.setLayout(option_widget_layout)

        function_select = QtWidgets.QComboBox()
        function_select.addItems(processes.keys())
        p_index = [n for n, i in enumerate(processes.keys()) if i == process.name][0]
        function_select.setCurrentIndex(p_index)
        function_select.currentIndexChanged \
            .connect(self._clicked_function_select)
        option_widget_layout.addWidget(function_select)

        option_box = QtWidgets.QFormLayout()
        process.display_settings_at(option_box)
        option_widget_layout.addLayout(option_box, 1)

        self._option_box.insertWidget(index, option_widget)
        # self._option_box.setCurrentIndex(index)
        self._chain_box.setCurrentRow(index)
        self.set_process(index, process)

    def delete_process(self, index: int):
        self._option_box.removeWidget(self._option_box.currentWidget())
        self._chain_box.takeItem(index)
    def set_process(self, index: int, process: ImageProcess):
        self._chain_box.item(index).setText(process.long_name())
        self._cleanup_options_box()
        process.display_settings_at(self.get_option_box())
        # update function select combo box in case this setter was
        # called programmatically
        function_select = self._option_box.currentWidget().layout() \
            .itemAt(0).widget()
        function_select.blockSignals(True)
        p_index = [n for n, i in enumerate(processes.keys()) if i == process.name][0]
        function_select.setCurrentIndex(p_index)
        function_select.blockSignals(False)
    def swap_processes(self, first: int, second: int):
        self._chain_box.insertItem(first, self._chain_box.takeItem(second))
        widget = self._option_box.widget(second)
        self._option_box.removeWidget(widget)
        self._option_box.insertWidget(first, widget)
        self._chain_box.setCurrentRow(first)
    def display_process(self, index: int):
        self._option_box.setCurrentIndex(index)
    def _cleanup_options_box(self):
        opts = self.get_option_box()
        while opts.count() > 0:
            opts.removeRow(0)
    def get_option_box(self):
        return self._option_box.currentWidget().layout().itemAt(1).layout()
    def get_func_select(self):
        return self._option_box.currentWidget().layout().itemAt(0).widget()

    def dialog_select_process_type(self):
        args = SimpleNamespace()
        dialog = NewProcessWindow(args, self.app_control)
        dialog.exec_()
        if dialog.result() == QtWidgets.QDialog.Accepted:
            return args.key
        else:
            return None

    def dialog_variable_view(self):
        dialog = VariableViewWindow(self.app_control)
        dialog.exec_()


class AppModel:
    def __init__(self, args):
        self.processes = []
        self.paused = False
        self.input_image = None
        self.input_type = None
        self.output_image = None
        self.output_type = None
        self.variable_container = VariableContainer()
        self.ai_model_collection = AiModelCollection(self.variable_container)

        if args.image: # TODO unify this with code used in load image func
            img = cv.cvtColor(cv.imread(args.image), cv.COLOR_BGR2RGB)
            tp = 'rgb'
            if args.channel:
                img = take_channel(img, args.channel)
                tp = 'gray'
            self.input_type = tp;
            self.input_image = img

        if args.template:
            for name, path in args.template:
                self.ai_model_collection.add_template(path, name)
            print('Loaded templates: ', self.ai_model_collection.templates.keys())

    def link(self, ctrl: AppControl):
        self.app_control = ctrl
    def update(self):
        if not self.paused:
            self.output_type, self.output_image = self.process(self.input_image)

    def process(self, image: np.array) -> Tuple[str, np.array]:
        if image is None:
            return (None, None)
        procedure = ProcedureMeta(image, self.ai_model_collection, self.variable_container)
        procedure.type = self.input_type
        backup_type = procedure.type
        try:
            for n, i in enumerate(self.processes):
                backup_type = procedure.type
                procedure.type = i.assert_type(procedure.type)
                i.apply_to_procedure(procedure)
                if not procedure.skip:
                    procedure.image = i.apply(procedure.image)
        except Exception as e:
            if isinstance(e, cv.error):
                # pylint: disable=no-member
                if str(e.err) == 'src.type() == CV_8UC1':
                    # TODO unknown channel count
                    e = ImageChannelCountException('unknown', 1, 3)
            traceback.print_exc()
            procedure.type = backup_type
            self.app_control.mark_error_at(n, i, e)
        return (procedure.type, procedure.image)
    def add_process(self, index: int, process: ImageProcess):
        self.processes.insert(index, process)
    def delete_process(self, index: int):
        self.processes.pop(index)
    def swap_processes(self, first: int, second: int):
        procs = self.processes
        procs[first], procs[second] = procs[second], procs[first]
    def get_process(self, index: int) -> ImageProcess:
        return self.processes[index]
    def set_process(self, index: int, process: ImageProcess):
        if index < len(self.processes):
            self.processes[index] = process
        else:
            self.processes.append(process)
    def toggle_paused(self):
        self.paused = not self.paused
    def set_input_image(self, image: cv.UMat):
        self.input_image = image

class AppControl:
    def __init__(self, ui: AppUi, model: AppModel, args):
        self.ui = ui
        self.model = model
        self.image_path = None
        self.procedure_path = None
        ui.link(self)
        model.link(self)
        self.draw_input_image()
        if args.procedure:
            self.open_procedure(in_place=False, file_name=args.procedure)
        else:
            self.update() # Simply draws output image
    def get_variable_container(self):
        return self.model.variable_container
    def add_new_select(self, index: QtCore.QModelIndex):
        key = self.ui.dialog_select_process_type()
        if key is not None:
            self.add_new(index)
            self.set_process(index.row() + 1, key) # TODO still PyQT randomly deleting my objects
    def add_new(self, index: QtCore.QModelIndex, process: ImageProcess = None):
        if process is None:
            process = NoopProcess(self)
        if isinstance(index, int):
            self.model.add_process(index + 1, process)
            self.ui.add_process(index + 1, process)
        elif index.isValid():
            self.model.add_process(index.row() + 1, process)
            self.ui.add_process(index.row() + 1, process)
        else:
            self.model.add_process(0, process)
            self.ui.add_process(0, process)
    def delete(self, index: QtCore.QModelIndex):
        if index.isValid():
            self.model.delete_process(index.row())
            self.ui.delete_process(index.row())
        else:
            self.model.delete_process(0)
            self.ui.delete_process(0)
    def move_up(self, index: QtCore.QModelIndex):
        if index.isValid() and index.row() > 0:
            self.model.swap_processes(index.row() - 1, index.row())
            self.ui.swap_processes(index.row() - 1, index.row())
    def move_down(self, index: QtCore.QModelIndex):
        if index.isValid() and index.row() < self.ui._chain_box.count() - 1:
            self.model.swap_processes(index.row() + 1, index.row())
            self.ui.swap_processes(index.row() + 1, index.row())
    def pause(self):
        self.model.toggle_paused()
    def select_chain_box_item(self):
        self.ui._chain_box.blockSignals(True)
        index = self.ui._chain_box.currentIndex()
        if index.isValid():
            index = index.row()
            self.ui.display_process(index)
        self.ui._chain_box.blockSignals(False)
    def set_process(self, index: QtCore.QModelIndex, name: str):
        # select.blockSignals(True)
        self.ui._chain_box.blockSignals(True)
        if isinstance(index, QtCore.QModelIndex) and index.isValid():
            index = index.row()
        if isinstance(index, QtCore.QModelIndex) and not index.isValid():
            return
        if name != '':
            process = processes[name](self, self.update)
            self.model.set_process(index, process)
            self.ui.set_process(index, process)
            self.ui.display_process(index)
        # select.blockSignals(False)
        self.ui._chain_box.blockSignals(False)
    def update(self):
        self.ui.update()
        self.model.update()
        self.draw_output_image()
    def draw_input_image(self):
        if self.model.input_image is not None:
            index = self.ui.current_input_tab()
            image_types[self.model.input_type].set_front(self.ui._input_image_frame)
            image_types[self.model.input_type].draw_input(self.model.input_image, index)
    def draw_output_image(self):
        if self.model.output_image is not None:
            index = self.ui.current_output_tab()
            image_types[self.model.output_type].set_front(self.ui._output_image_frame)
            image_types[self.model.output_type].draw_output(self.model.output_image, index)
    def mark_error_at(self, index: int, proc: ImageProcess, e: Exception):
        self.ui.mark_error_at(index, proc, e)
    def set_input_image(self, image: cv.UMat):
        self.model.set_input_image(image)
        self.draw_input_image()
        self.update()
    def get_output_image(self) -> cv.UMat:
        return self.model.output_image
    def get_process(self, index: int) -> ImageProcess:
        return self.model.get_process(index)
    def open_image(self, in_place: bool = False):
        prompt = 'Open image in place' if in_place else 'Open image'
        file_name, _ = QtWidgets.QFileDialog.getOpenFileName(
            self.ui, prompt, os.getcwd(), image_types['rgb'].read_hint())
        if file_name != '':
            img = cv.cvtColor(cv.imread(file_name), cv.COLOR_BGR2RGB)
            tp = 'rgb'
            if len(self.model.processes) == 0:
                args = SimpleNamespace()
                dialog = OpenImageSelectChannelWindow(args)
                dialog.exec_()
                if dialog.result() == QtWidgets.QDialog.Accepted:
                    tp, img = dialog.preprocess_image(img);
                    dialog.generate_procedure(self)
                else:
                    return
            self.model.input_type = tp;
            self.set_input_image(img)
            if in_place:
                self.image_path = file_name
    def batch_process_images(self, in_place: bool = False):
        prompt = 'Open working directory' if in_place else 'Open source directory'
        src = QtWidgets.QFileDialog.getExistingDirectory(
            self.ui, prompt, os.getcwd())
        if src == '':
            return None
        src_files = os.listdir(src)
        if len(src_files) == 0:
            QtWidgets.QErrorMessage().showMessage('No files to process found!')
            return None
        if in_place:
            dst = src
        else:
            dst = QtWidgets.QFileDialog.getExistingDirectory(
                self.ui, 'Open destination directory', os.getcwd())
            if dst == '':
                return None
        suffix = QtWidgets.QInputDialog.getText(
            self.ui, 'Batch output file suffix',
            ('Enter a suffix to be appended to output files.\n'
             'Leaving this empty, if source and destination directory\n'
             'are the same (or you selected working directory), will\n'
             'lead to overwriting original images.'),
            text='-output'
        )
        if not suffix[1]:
            return None
        suffix = suffix[0]
        dst_files = [
            os.path.splitext(i)[0] + suffix# + os.path.splitext(i)[1]
            for i in src_files
        ]
        file_pairs = zip(src_files, dst_files)
        for (src_file, dst_file) in file_pairs:
            img = cv.imread(os.path.join(src, src_file))
            if img is not None:
                file_type = image_types[self.model.output_type]
                suffix_hint, *_ = file_type.write_hint()
                dst_path = os.path.join(dst, dst_file + suffix_hint)
                tp, p_img = self.model.process(img)
                image_types[tp].write(dst_path, p_img)
        message = QtWidgets.QMessageBox()
        message.setIcon(QtWidgets.QMessageBox.Information)
        message.setText('Done batch processing %d files!' % len(dst_files))
        message.setStandardButtons(QtWidgets.QMessageBox.Ok)
        message.show()
    def save_image(self, save_as: bool = False):
        prompt = 'Save image as' if save_as else 'Save image'
        file_name = self.image_path
        if file_name is None or save_as:
            file_type = image_types[self.model.output_type]
            suffix_hint, filter_hint = file_type.write_hint()
            file_hint = 'output' + suffix_hint
            file_name, _ = QtWidgets.QFileDialog.getSaveFileName(
                self.ui, prompt, os.path.join(os.getcwd(), file_hint), filter_hint)
        if file_name != '':
            img = self.get_output_image()
            image_types[self.model.output_type].write(file_name, img)
            self.image_path = file_name
    def open_procedure(self, in_place: bool = False, file_name: str = None):
        if file_name is None:
            # the _ here may be replaced by 'v' should more formats be released
            prompt = 'Open procedure in place' if in_place else 'Open procedure'
            file_name, _ = QtWidgets.QFileDialog.getOpenFileName(
                self.ui, prompt, os.getcwd(),
                'Procedure v1 files (*.v1.csv)')
        if file_name != '':
            # pause updates
            was_paused = self.model.paused
            if not was_paused:
                self.model.toggle_paused()
            # clean previous
            while len(self.model.processes) > 0:
                self.model.delete_process(0)
                self.ui.delete_process(0)
            # load new
            with open(file_name, 'r') as file:
                for n, (name, *vals) in enumerate(csv.reader(file)):
                    process = processes[name](self, self.update)
                    self.model.add_process(n, process)
                    process.load(vals)
                    # TODO improve process addition - right now, a workaround
                    # is required, as PyQt loves to delete my widgets between
                    # creation and addition
                    self.ui.add_process(n, NoopProcess(self))
                    self.ui.set_process(n, process)
            # resume updates
            if not was_paused:
                self.model.toggle_paused()
            if in_place:
                self.procedure_path = file_name
            self.update()
    def save_procedure(self, save_as: bool = False):
        # the _ here may be replaced by 'v' should more formats be released
        prompt = 'Save procedure as' if save_as else 'Save procedure'
        file_name = self.procedure_path
        if file_name is None or save_as:
            file_name, _ = QtWidgets.QFileDialog.getSaveFileName(
                self.ui, prompt, os.getcwd(),
                'Procedure v1 files (*.v1.csv)')
        if file_name != '':
            with open(file_name, 'w') as file:
                writer = csv.writer(file)
                for n, process in enumerate(self.model.processes):
                    writer.writerow([process.name] + process.save())
            self.procedure_path = file_name
    def add_variable(self, ui: AppUi):
        name_result = QtWidgets.QInputDialog.getText(
            ui, 'Image variable name',
            'Enter a name for the image variable.',
            text='IMAGE'
        )
        if not name_result[1]:
            return None
        name = name_result[0]
        self.model.variable_container._image[name] = None

class ProcedureMeta:
    def __init__(self, image: cv.UMat, models: AiModelCollection, vc: VariableContainer):
        self.input_image = image
        self.image = image
        self.skip = False
        self.storage = vc._image
        for k in self.storage.keys():
            self.storage[k] = None
        self.models = models
        self.type = 'rgb'

class AppLayout:
    def __init__(self, name: str):
        self.name = name
        self.chain_box_rect = ()
        self.option_box_rect = ()
        self.input_image_rect = ()
        self.output_image_rect = ()
    def set_column_stretches(self, grid: QtWidgets.QGridLayout):
        raise NotImplementedError("Please Implement this method")
    def reset_column_stretches(self, grid: QtWidgets.QGridLayout):
        raise NotImplementedError("Please Implement this method")


class ToolsLeftImagesRightLayout(AppLayout):
    def __init__(self):
        super().__init__('Tools left, images right')
        self.chain_box_rect = (0, 0, 1, 1)
        self.option_box_rect = (1, 0, 1, 1)
        self.input_image_rect = (0, 1, 1, 1)
        self.output_image_rect = (1, 1, 1, 1)
    def set_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setColumnMinimumWidth(0, 300)
        grid.setRowMinimumHeight(0, 300)
        grid.setRowMinimumHeight(1, 300)
        grid.setColumnStretch(1, 1)
    def reset_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setColumnMinimumWidth(0, 0)
        grid.setRowMinimumHeight(0, 0)
        grid.setRowMinimumHeight(1, 0)
        grid.setColumnStretch(1, 0)

class HorizontalLayout(AppLayout):
    def __init__(self):
        super().__init__('Tools, then images, horizontally')
        self.chain_box_rect = (0, 0, 1, 1)
        self.option_box_rect = (0, 1, 1, 1)
        self.input_image_rect = (0, 2, 1, 1)
        self.output_image_rect = (0, 3, 1, 1)
    def set_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setColumnMinimumWidth(0, 300)
        grid.setColumnMinimumWidth(1, 300)
        grid.setColumnStretch(2, 1)
        grid.setColumnStretch(3, 1)
    def reset_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setColumnMinimumWidth(0, 0)
        grid.setColumnMinimumWidth(1, 0)
        grid.setColumnStretch(2, 0)
        grid.setColumnStretch(3, 0)

class VerticalLayout(AppLayout):
    def __init__(self):
        super().__init__('Images, then tools, vertically')
        self.input_image_rect = (0, 0, 1, 1)
        self.output_image_rect = (1, 0, 1, 1)
        self.chain_box_rect = (2, 0, 1, 1)
        self.option_box_rect = (3, 0, 1, 1)
    def set_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setColumnMinimumWidth(0, 300)
        grid.setRowMinimumHeight(2, 300)
        grid.setRowMinimumHeight(3, 300)
        grid.setRowStretch(0, 1)
        grid.setRowStretch(1, 1)
    def reset_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setColumnMinimumWidth(0, 0)
        grid.setRowMinimumHeight(2, 0)
        grid.setRowMinimumHeight(3, 0)
        grid.setRowStretch(0, 1)
        grid.setRowStretch(1, 1)

class VerticalHorToolsLayout(AppLayout):
    def __init__(self):
        super().__init__('Images, then tools horizontally, all verticelly')
        self.chain_box_rect = (2, 0, 1, 1)
        self.option_box_rect = (2, 1, 1, 1)
        self.input_image_rect = (0, 0, 1, 2)
        self.output_image_rect = (1, 0, 1, 2)
    def set_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setRowMinimumHeight(2, 300)
        grid.setColumnMinimumWidth(0, 300)
        grid.setColumnMinimumWidth(1, 300)
        grid.setRowStretch(0, 1)
        grid.setRowStretch(1, 1)
    def reset_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setRowMinimumHeight(2, 300)
        grid.setColumnMinimumWidth(0, 0)
        grid.setColumnMinimumWidth(1, 0)
        grid.setRowStretch(0, 0)
        grid.setRowStretch(1, 0)

class HorizontalVerToolsLayout(AppLayout):
    def __init__(self):
        super().__init__('Tools vertically, images, all horizontally')
        self.chain_box_rect = (0, 0, 1, 1)
        self.option_box_rect = (1, 0, 1, 1)
        self.input_image_rect = (0, 1, 2, 1)
        self.output_image_rect = (0, 2, 2, 1)
    def set_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setRowMinimumHeight(0, 300)
        grid.setRowMinimumHeight(1, 300)
        grid.setColumnMinimumWidth(0, 300)
        grid.setColumnStretch(1, 1)
        grid.setColumnStretch(2, 1)
    def reset_column_stretches(self, grid: QtWidgets.QGridLayout):
        grid.setRowMinimumHeight(0, 0)
        grid.setRowMinimumHeight(1, 0)
        grid.setColumnMinimumWidth(0, 0)
        grid.setColumnStretch(1, 0)
        grid.setColumnStretch(2, 0)

"""
Model collection
"""

class AiModelCollection:
    def __init__(self, vc: VariableContainer):
        self.templates = vc._template
        # self.patterns = dict()
        self.learning_data = dict()
        self.recognition_model = dict()
    def open_template(self, ui: AppUi):
        prompt = 'Open template file'
        template_file, _ = QtWidgets.QFileDialog.getOpenFileName(
            ui,
            prompt,
            os.getcwd(),
            ';;'.join(i.read_hint() for i in image_types.values() if i.read_hint())
        )
        if template_file == '':
            return None
        model_name_result = QtWidgets.QInputDialog.getText(
            ui, 'Template name',
            ('Enter a name for the loaded template. Using a previously\n'
             'used template name will overwrite old template in memory.'),
            text='TEMPLATE'
        )
        if not model_name_result[1]:
            return None
        name = model_name_result[0]
        success, tp = self.add_template(template_file, name)
        if not success:
            if tp is None:
                message = QtWidgets.QMessageBox()
                message.setIcon(QtWidgets.QMessageBox.Warning)
                message.setText('Could not load template: Type not recognized')
                message.setStandardButtons(QtWidgets.QMessageBox.Ok)
                message.exec()
                return
            message = QtWidgets.QMessageBox()
            message.setIcon(QtWidgets.QMessageBox.Warning)
            message.setText('Could not load template of type %s' % tp.name)
            message.setStandardButtons(QtWidgets.QMessageBox.Ok)
            message.exec()
            return
        message = QtWidgets.QMessageBox()
        message.setIcon(QtWidgets.QMessageBox.Information)
        message.setText('Loaded template of type %s' % tp.name)
        message.setStandardButtons(QtWidgets.QMessageBox.Ok)
        message.exec()
    def add_template(self, path: str, name: str):
        img = None
        tp_id = None
        tp = None
        for type_name, t in image_types.items():
            if t.can_read(path):
                tp_id = type_name
                tp = t
                img = t.read(path)
                break
        if img is not None:
            self.templates[name] = (img, tp_id)
        return img is not None, tp
    def open_learning_files(self, ui: AppUi):
        prompt = 'Open learning data directory'
        patterns = QtWidgets.QFileDialog.getExistingDirectory(ui, prompt, os.getcwd())
        if patterns == '':
            return None
        files = os.listdir(patterns)
        if len(files) == 0:
            QtWidgets.QErrorMessage().showMessage('No files to process found!')
            return None
        model_name_result = QtWidgets.QInputDialog.getText(
            ui, 'Learning data set name',
            ('Enter a name for the loaded data set. Using a previously\n'
              'used data set name will overwrite old set in memory.'),
            text='DATASET'
        )
        if not model_name_result[1]:
            return None
        model_name = model_name_result[0]
        successes = 0
        self.learning_data[model_name] = []
        for file in files:
            img = cv.imread(os.path.join(patterns, file))
            if img is not None:
                successes += 1
                self.learning_data[model_name].append(img)
        message = QtWidgets.QMessageBox()
        message.setIcon(QtWidgets.QMessageBox.Information)
        message.setText('Successfully loaded %d templates!' % successes)
        message.setStandardButtons(QtWidgets.QMessageBox.Ok)
        message.show()
    def open_recognition_model(self, ui: AppUi):
        prompt = 'Open recognition model'
        file_name = QtWidgets.QFileDialog.getOpenFileName(ui, prompt, os.getcwd())[0]
        if file_name == '':
            return None
        model_name_result = QtWidgets.QInputDialog.getText(
            ui, 'Recognition model name',
            ('Enter a name for the loaded recognition model. Using a\n'
              'previously used model name will overwrite old model in memory.'),
            text='MODEL'
        )
        if not model_name_result[1]:
            return None
        model_name = model_name_result[0]
        self.recognition_model[model_name] = self.process_model(file_name)
        message = QtWidgets.QMessageBox()
        message.setIcon(QtWidgets.QMessageBox.Information)
        message.setText('Successfully loaded recognition model!')
        message.setStandardButtons(QtWidgets.QMessageBox.Ok)
        message.show()
    def process_model(self, filename: str):
        return dlib.shape_predictor(filename)

"""
Utilities
"""

def spectrogram(im, reverse: bool=False):
    c = -2j if not reverse else 2j
    u = np.arange(im.shape[0])
    v = np.arange(im.shape[1])
    interm = (im[None, ...] * np.exp(c * np.pi * v[None, None, :] * v[:, None, None] / im.shape[1])).mean(axis=2)
    ft = (interm[None, ...] * np.exp(c * np.pi * u[None, None, :] * u[:, None, None] / im.shape[0])).mean(axis=2)
    # ft_log = np.log(np.abs(ft))
    return ft

def take_channel(image, channel):
    return {
        'R': lambda img : img[:,:,0],
        'G': lambda img : img[:,:,1],
        'B': lambda img : img[:,:,2],
        'Y': lambda img : cv.cvtColor(img, cv.COLOR_BGR2YCrCb)[:,:,0],
        'Cb': lambda img : cv.cvtColor(img, cv.COLOR_BGR2YCrCb)[:,:,1],
        'Cr': lambda img : cv.cvtColor(img, cv.COLOR_BGR2YCrCb)[:,:,2],
        'H': lambda img : cv.cvtColor(img, cv.COLOR_BGR2HSV)[:,:,0],
        'S': lambda img : cv.cvtColor(img, cv.COLOR_BGR2HSV)[:,:,1],
        'V': lambda img : cv.cvtColor(img, cv.COLOR_BGR2HSV)[:,:,2],
        'gray': lambda img : cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    }[channel](image)

class JsonAdapter:
    'A collection of encoders and decoders to serialize non-serializable objects.'
    def __init__(self, type_key: str='$type'):
        self.encoders = {}
        self.decoders = {}
        self.key = type_key
    def with_type(
            self,
            type_: Type,
            encoder: Callable[[object], Dict[str, object]],
            decoder: Callable[[Dict[str, object]], object]
        ) -> JsonAdapter:
        'Registers an encoder and a decoder for the given type, and returns self for chaining.'
        if type_ in self.encoders:
            raise Exception(f'Type is already registered: {type.__name__}')
        # Type will be readily available when encoding; str when decoding
        self.encoders[type_] = encoder
        self.decoders[str(type_)] = decoder
        return self
    def encode(self, obj: object) -> Dict[str, object]:
        'Usage: json.dump(... default=adapter.encode) or JSONEncoder(... default=adapter.encode)'
        type_ = type(obj)
        if type_ in self.encoders:
            encoder = self.encoders[type_]
            return encoder(obj) | {self.key: str(type_)}
        raise TypeError(f'Object of type {obj.__class__.__name__} is not JSON serializable')
    def decode(self, obj: Dict[str, object]) -> object | Dict[str, object]:
        'Usage: json.load(... object_hook=adapter.decode) or JSONEncoder(... object_hook=adapter.decode)'
        if self.key in obj and obj[self.key] in self.decoders:
            decoder = self.decoders[obj[self.key]]
            return decoder(obj)
        return obj

class VariableContainer:
    def __init__(self):
        self._image = VariableDict(self._receive_image_update, None, self._receive_image_update)
        self._template = VariableDict(self._receive_template_update, None, self._receive_template_update)
        self._image_hooks = []
        self._template_hooks = []
    def hook_image(self, func):
        self._image_hooks.append(func)
    def hook_template(self, func):
        self._template_hooks.append(func)
    def _receive_image_update(self, *args):
        for func in self._image_hooks:
            func(self._image)
    def _receive_template_update(self, *args):
        for func in self._template_hooks:
            func(self._template)

class VariableDict(dict):
    def __init__(self, on_add, on_mut, on_del):
        self._on_add = on_add
        self._on_mut = on_mut
        self._on_del = on_del
    def __setitem__(self, key, value):
        added = key not in self
        super().__setitem__(key, value)
        if added:
            if self._on_add is not None:
                self._on_add(key, value)
        else:
            if self._on_mut is not None:
                self._on_mut(key, value)
    def __delitem__(self, key):
        super().__delitem__(key)
        if self._on_del is not None:
            self._on_del(key)


"""
Image types
"""

class ImageType:
    def __init__(self, name: str):
        self.name = name
        self.output_widget = None
        self.output_index = -1
        self.input_widget = None
        self.input_index = -1
    def create_input_tab_widget(self, at: QtWidgets.QStackedWidget) -> QtWidgets.QTabWidget:
        self.input_widget = QtWidgets.QTabWidget(at)
        self._populate_input_tab_widget(self.input_widget)
        self.input_index = at.addWidget(self.input_widget)
        return self.input_widget
    def create_output_tab_widget(self, at: QtWidgets.QStackedWidget) -> QtWidgets.QTabWidget:
        self.output_widget = QtWidgets.QTabWidget(at)
        self._populate_output_tab_widget(self.output_widget)
        self.output_index = at.addWidget(self.output_widget)
        return self.output_widget
    def _populate_input_tab_widget(self, tab: QtWidgets.QTabWidget) -> None:
        raise NotImplementedError("Please Implement this method")
    def _populate_output_tab_widget(self, tab: QtWidgets.QTabWidget) -> None:
        raise NotImplementedError("Please Implement this method")
    def _create_pyplot_page(self, name: str, tab: QtWidgets.QTabWidget) -> (int, Figure, FigureCanvas, Axes):
        page = QtWidgets.QWidget(tab)
        page_layout = QtWidgets.QVBoxLayout()
        page.setLayout(page_layout)
        figure = Figure()
        canvas = FigureCanvas(figure)
        toolbar = NavigationToolbar(canvas, tab)
        page_layout.addWidget(toolbar)
        page_layout.addWidget(canvas)
        index = tab.addTab(page, QtGui.QIcon(), name)
        ax = figure.add_subplot(111)
        return (index, figure, canvas, ax)
    def can_accept(self, img_type: str) -> bool:
        return image_types[img_type] == self
    def set_front(self, frame: QtWidgets.QStackedWidget):
        frame.setCurrentIndex(self.output_index)
    def get_input_tab_widget(self) -> QtWidgets.QTabWidget:
        return self.input_widget
    def get_output_tab_widget(self) -> QtWidgets.QTabWidget:
        return self.output_widget
    def draw_input(self, subject: np.array, tab_index: int) -> None:
        raise NotImplementedError("Please Implement this method")
    def draw_output(self, subject: np.array, tab_index: int) -> None:
        raise NotImplementedError("Please Implement this method")
    def write(self, path: str, subject):
        raise NotImplementedError("Please Implement this method")
    def write_hint(self):
        return ('.jpg', 'Image files (*.jpg *.png)')
    def read(self, path: str):
        raise NotImplementedError("Please Implement this method")
    def read_hint(self):
        return None
    def can_read(self, path: str):
        return False

class RgbType(ImageType):
    def __init__(self):
        self.input_image_figure = None
        self.input_image_canvas = None
        self.input_image_axes = None
        self.input_image_index = -1
        self.output_image_figure = None
        self.output_image_canvas = None
        self.output_image_axes = None
        self.output_image_index = -1
        super().__init__('color image')
    def _populate_input_tab_widget(self, tab: QtWidgets.QTabWidget):
        self.input_image_index, self.input_image_figure, self.input_image_canvas, self.input_image_axes \
            = self._create_pyplot_page('Image view', tab)
    def _populate_output_tab_widget(self, tab: QtWidgets.QTabWidget):
        self.output_image_index, self.output_image_figure, self.output_image_canvas, self.output_image_axes \
            = self._create_pyplot_page('Image view', tab)
    def draw_input(self, subject: np.array, tab_index: int) -> None:
        ax = self.input_image_axes
        ax.clear()
        ax.imshow(subject)
        plt.axis('off')
        self.input_image_canvas.draw()
    def draw_output(self, subject: np.array, tab_index: int) -> None:
        ax = self.output_image_axes
        ax.clear()
        ax.imshow(subject)
        plt.axis('off')
        self.output_image_canvas.draw()
    def write(self, path: str, subject):
        cv.imwrite(path, cv.cvtColor(subject, cv.COLOR_BGR2RGB))
    def read(self, path):
        return cv.cvtColor(cv.imread(path), cv.COLOR_BGR2RGB)
    def read_hint(self):
        return 'Image files (*.jpg *.png)'
    def can_read(self, path: str):
        return path.endswith('.jpg') or path.endswith('.png')

class GrayType(ImageType):
    def __init__(self):
        # self.input_image_figure = None
        self.input_colormap_figure = None
        self.input_histogram_figure = None
        self.input_spectrogram_figure = None
        # self.input_image_canvas = None
        self.input_colormap_canvas = None
        self.input_histogram_canvas = None
        self.input_spectrogram_canvas = None
        # self.input_image_axes = None
        self.input_colormap_axes = None
        self.input_histogram_axes = None
        self.input_spectrogram_axes = None
        # self.input_image_index = -1
        self.input_colormap_index = -1
        self.input_histogram_index = -1
        self.input_spectrogram_index = -1

        # self.output_image_figure = None
        self.output_colormap_figure = None
        self.output_histogram_figure = None
        self.output_spectrogram_figure = None
        # self.output_image_canvas = None
        self.output_colormap_canvas = None
        self.output_histogram_canvas = None
        self.output_spectrogram_canvas = None
        # self.output_image_axes = None
        self.output_colormap_axes = None
        self.output_histogram_axes = None
        self.output_spectrogram_axes = None
        # self.output_image_index = -1
        self.output_colormap_index = -1
        self.output_histogram_index = -1
        self.output_spectrogram_index = -1

        self.input_colormap_colorbar = None
        self.output_colormap_colorbar = None
        self.input_spectrogram_colorbar = None
        self.output_spectrogram_colorbar = None
        self.input_colormap_mappable = None
        self.output_colormap_mappable = None
        self.input_spectrogram_mappable = None
        self.output_spectrogram_mappable = None
        super().__init__('grayscale image')
    def can_accept(self, img_type: str) -> bool:
        return img_type == 'bin' or super().can_accept(img_type)
    def _populate_input_tab_widget(self, tab: QtWidgets.QTabWidget):
        # self.input_image_index, self.input_image_figure, self.input_image_canvas \
        #     = self._create_pyplot_page('Image view', tab)
        self.input_colormap_index, self.input_colormap_figure, self.input_colormap_canvas, self.input_colormap_axes \
            = self._create_pyplot_page('Colormap view', tab)
        self.input_histogram_index, self.input_histogram_figure, self.input_histogram_canvas, self.input_histogram_axes \
            = self._create_pyplot_page('Histogram view', tab)
        self.input_spectrogram_index, self.input_spectrogram_figure, self.input_spectrogram_canvas, self.input_spectrogram_axes \
            = self._create_pyplot_page('Spectrogram view', tab)
    def _populate_output_tab_widget(self, tab: QtWidgets.QTabWidget):
        # self.output_image_index, self.output_image_figure, self.output_image_canvas \
        #     = self._create_pyplot_page('Image view', tab)
        self.output_colormap_index, self.output_colormap_figure, self.output_colormap_canvas, self.output_colormap_axes \
            = self._create_pyplot_page('Colormap view', tab)
        self.output_histogram_index, self.output_histogram_figure, self.output_histogram_canvas, self.output_histogram_axes \
            = self._create_pyplot_page('Histogram view', tab)
        self.output_spectrogram_index, self.output_spectrogram_figure, self.output_spectrogram_canvas, self.output_spectrogram_axes \
            = self._create_pyplot_page('Spectrogram view', tab)
    def draw_input(self, subject: np.array, tab_index: int) -> None:
        # if tab_index == self.input_image_index:
        #     ax = self.input_image_figure.add_subplot(111)
        #     ax.clear()
        #     ax.imshow(subject, cmap='gray')
        #     plt.axis('off')
        #     self.input_image_canvas.draw()
        if tab_index == self.input_colormap_index:
            ax = self.input_colormap_axes
            ax.clear()
            self.input_colormap_mappable = ax.imshow(subject, cmap='jet')
            if self.input_colormap_colorbar is None:
                self.input_colormap_colorbar = self.input_colormap_figure.colorbar(self.input_colormap_mappable, ax=ax)
            self.input_colormap_colorbar.update_normal(self.input_colormap_mappable)
            plt.axis('off')
            self.input_colormap_canvas.draw()
        if tab_index == self.input_histogram_index:
            ax = self.input_histogram_axes
            ax.clear()
            ax.hist(subject.reshape(-1), bins=range(256))
            plt.axis('off')
            self.input_histogram_canvas.draw()
        if tab_index == self.input_spectrogram_index:
            ax = self.input_spectrogram_axes
            ax.clear()
            # ft = np.fft.fftshift(np.log(np.abs(spectrogram(subject))))
            ft = np.fft.fftshift(np.log(np.abs(np.fft.fft2(subject.astype(np.float32) / 127.5 - 1))))
            self.input_spectrogram_mappable = ax.imshow(ft, cmap='jet')
            if self.input_spectrogram_colorbar is None:
                self.input_spectrogram_colorbar = self.input_spectrogram_figure.colorbar(self.input_spectrogram_mappable, ax=ax)
            self.input_spectrogram_colorbar.update_normal(self.input_spectrogram_mappable)
            plt.axis('off')
            self.input_spectrogram_canvas.draw()
    def draw_output(self, subject: np.array, tab_index: int) -> None:
        # if tab_index == self.output_image_index:
        #     ax = self.output_image_figure.add_subplot(111)
        #     ax.clear()
        #     ax.imshow(subject, cmap='gray')
        #     plt.axis('off')
        #     self.output_image_canvas.draw()
        if tab_index == self.output_colormap_index:
            ax = self.output_colormap_axes
            ax.clear()
            self.output_colormap_mappable = ax.imshow(subject, cmap='jet')
            if self.output_colormap_colorbar is None:
                self.output_colormap_colorbar = self.output_colormap_figure.colorbar(self.output_colormap_mappable, ax=ax)
            self.output_colormap_colorbar.update_normal(self.output_colormap_mappable)
            plt.axis('off')
            self.output_colormap_canvas.draw()
        if tab_index == self.output_histogram_index:
            ax = self.output_histogram_axes
            ax.clear()
            ax.hist(subject.reshape(-1), bins=range(256))
            plt.axis('off')
            self.output_histogram_canvas.draw()
        if tab_index == self.output_spectrogram_index:
            ax = self.output_spectrogram_axes
            ax.clear()
            # ft = np.fft.fftshift(np.log(np.abs(spectrogram(subject))))
            ft = np.fft.fftshift(np.log(np.abs(np.fft.fft2(subject.astype(np.float32) / 127.5 - 1))))
            self.output_spectrogram_mappable = ax.imshow(ft, cmap='jet')
            if self.output_spectrogram_colorbar is None:
                self.output_spectrogram_colorbar = self.output_spectrogram_figure.colorbar(self.output_spectrogram_mappable, ax=ax)
            self.output_spectrogram_colorbar.update_normal(self.output_spectrogram_mappable)
            plt.axis('off')
            self.output_spectrogram_canvas.draw()
    def write(self, path: str, subject):
        cv.imwrite(path, subject)

class BinType(ImageType):
    def __init__(self):
        self.image_figure = None
        self.image_canvas = None
        self.image_axes = None
        self.image_index = None
        super().__init__('binary image')
    def _populate_input_tab_widget(self, tab: QtWidgets.QTabWidget):
        pass
    def _populate_output_tab_widget(self, tab: QtWidgets.QTabWidget):
        self.image_index, self.image_figure, self.image_canvas, self.image_axes \
            = self._create_pyplot_page('Image view', tab)
    def draw_input(self, subject: np.array, tab_index: int) -> None:
        pass
    def draw_output(self, subject: np.array, tab_index: int) -> None:
        ax = self.image_axes
        ax.clear()
        ax.imshow(subject, cmap='gray')
        plt.axis('off')
        self.image_canvas.draw()
    def write(self, path: str, subject):
        cv.imwrite(path, subject)

class KeyType(ImageType):
    def __init__(self):
        self.hueshift_figure = None
        # self.colormap_figure = None
        self.watershed_figure = None
        self.histogram_figure = None
        self.hueshift_canvas = None
        # self.colormap_canvas = None
        self.watershed_canvas = None
        self.histogram_canvas = None
        self.hueshift_index = -1
        # self.colormap_axes = None
        self.watershed_axes = None
        self.histogram_axes = None
        self.hueshift_axes = -1
        # self.colormap_index = -1
        self.watershed_index = -1
        self.histogram_index = -1

        self._colorbar_init = False
        super().__init__('grayscale image')
    def _populate_input_tab_widget(self, tab: QtWidgets.QTabWidget):
        pass
    def _populate_output_tab_widget(self, tab: QtWidgets.QTabWidget):
        self.hueshift_index, self.hueshift_figure, self.hueshift_canvas, self.hueshift_axes \
            = self._create_pyplot_page('Hueshift view', tab)
        self.watershed_index, self.watershed_figure, self.watershed_canvas, self.watershed_axes \
            = self._create_pyplot_page('Watershed view', tab)
        self.histogram_index, self.histogram_figure, self.histogram_canvas, self.histogram_axes \
            = self._create_pyplot_page('Histogram view', tab)
    def draw_input(self, subject: np.array, tab_index: int) -> None:
        pass
    def draw_output(self, subject: np.array, tab_index: int) -> None:
        if tab_index == self.hueshift_index:
            img = np.zeros(np.append(subject.shape, 3), dtype=np.uint8)
            colors = np.uint8(subject * 180 / (subject.max() + 1))
            img[:, :, 0] = colors
            img[:, :, 1] = 255
            img[:, :, 2] = 255 * (subject > 0)
            ax = self.hueshift_axes
            ax.clear()
            ax.imshow(cv.cvtColor(img, cv.COLOR_HSV2RGB))
            plt.axis('off')
            self.hueshift_canvas.draw()
        if tab_index == self.watershed_index:
            img = np.zeros(np.append(subject.shape, 3), dtype=np.uint8)
            colors = np.uint8(subject * 180 / (subject.max() + 1))
            img[:, :, 0] = colors
            img[:, :, 1] = 255
            img[:, :, 2] = 128 * (subject > 1) + 127 * (subject > 0)
            ax = self.watershed_axes
            ax.clear()
            ax.imshow(cv.cvtColor(img, cv.COLOR_HSV2RGB))
            plt.axis('off')
            self.watershed_canvas.draw()
        if tab_index == self.histogram_index:
            ax = self.histogram_axes
            ax.clear()
            vector = subject.reshape(-1)
            p = ax.hist(np.delete(vector, vector == 0), bins=range(1, subject.max() + 1))[2]
            mcol = subject.max() + 1
            colors = range(1, mcol) / mcol
            for col, patch in zip(colors, p):
                plt.setp(patch, facecolor=self._v_to_rgb(col))
            plt.axis('off')
            self.histogram_canvas.draw()
    def write(self, path: str, subject):
        cv.imwrite(path)
    def _v_to_rgb(self, v: float) -> np.array:
        if v < 1/6:
            return np.array([1, v * 6, 0, 1])
        if v < 2/6:
            return np.array([(v - 1/6) * 6, 1, 0, 1])
        if v < 3/6:
            return np.array([0, 1, (v - 2/6) * 6, 1])
        if v < 4/6:
            return np.array([0, (v - 3/6) * 6, 1, 1])
        if v < 5/6:
            return np.array([(v - 4/6) * 6, 0, 1, 1])
        return np.array([1, 0, (v - 5/6) * 6, 1])

class DatType(ImageType):
    def __init__(self):
        self.dump_figure = None
        self.adapter = JsonAdapter() \
            .with_type(cv.KeyPoint,
                encoder = lambda obj: {
                    'x': obj.pt[0],
                    'y': obj.pt[1],
                    'size': obj.size,
                    'angle': obj.angle,
                    'response': obj.response,
                    'class_id': obj.class_id,
                },
                decoder = lambda obj: cv.KeyPoint(
                    obj['x'],
                    obj['y'],
                    obj['size'],
                    obj['angle'],
                    obj['response'],
                    obj['class_id'],
                ),
            ) \
            .with_type(np.ndarray,
                encoder = lambda obj: {'data': obj.tolist()},
                decoder = lambda obj: np.array(obj['data']),
            ) \
            .with_type(complex,
                encoder = lambda obj: {'real': obj.real, 'imag': obj.imag},
                decoder = lambda obj: obj['real'] + obj['imag'] * 1j,
            )

        super().__init__('data namespace')
    def _populate_input_tab_widget(self, tab: QtWidgets.QTabWidget):
        pass
    def _populate_output_tab_widget(self, tab: QtWidgets.QTabWidget):
        self.dump_figure = QtWidgets.QPlainTextEdit(tab)
        self.dump_figure.setReadOnly(True)
        self.dump_figure.setFont(QtGui.QFont('Monospace'))
        tab.addTab(self.dump_figure, QtGui.QIcon(), 'Data dump')
    def draw_input(self, subject: np.array, tab_index: int) -> None:
        pass
    def draw_output(self, subject: np.array, tab_index: int) -> None:
        self.dump_figure.setPlainText(pformat(subject, indent=4, sort_dicts=False))
    def write(self, path: str, subject):
        with open(path, 'w') as file:
            json.dump(subject, file, default=self.adapter.encode)
    def write_hint(self):
        return ('.json', 'JSON files (*.json)')
    def read(self, path):
        with open(path, 'r') as file:
            return json.load(file, object_hook=self.adapter.decode)
    def read_hint(self):
        return 'JSON files (*.json)'
    def can_read(self, path: str):
        return path.endswith('.json')

"""
Processing functions
"""

def processing_action_colorspace(subject, params):
    # return cv.cvtColor(subject, params.f_conv)
    for (f_name, f_to_execute) in colorspace_conversion_funcs.items():
        if f_name == params.f_conv:
            return cv.cvtColor(subject, f_to_execute)
    raise ParamException('colorspace', 'conversion func', params.f_conv)

def processing_action_channel(subject, params):
    return take_channel(subject, params.channel.split(maxsplit=1)[0])

def processing_action_inrange(subject, params):
    lower = np.array(params.lower)
    upper = np.array(params.upper)
    chans = subject.shape[2] if len(subject.shape) >= 3 else 1
    if len(lower) != chans:
        raise ChannelCountException('inrange lower', chans, len(lower))
    if len(upper) != chans:
        raise ChannelCountException('inrange upper', chans, len(upper))
    mask = cv.inRange(subject, lower, upper)
    return cv.bitwise_and(subject, subject, mask=mask)

def processing_action_inrangeotherwise(subject, params):
    lower = np.array(params.lower)
    upper = np.array(params.upper)
    other = np.array(params.other)
    chans = subject.shape[2] if len(subject.shape) >= 3 else 1
    if len(lower) != chans:
        raise ChannelCountException('inrange lower', chans, len(lower))
    if len(upper) != chans:
        raise ChannelCountException('inrange upper', chans, len(upper))
    if len(other) != chans:
        raise ChannelCountException('inrange other', chans, len(other))
    mask = cv.inRange(subject, lower, upper)
    result_img = cv.bitwise_and(subject, subject, mask=mask)
    mask = cv.bitwise_not(mask) >= 1
    result_img[mask] = other
    return result_img

def processing_action_thresh(subject, params):
    if len(subject.shape) >= 3 and subject.shape[2] != 1:
        raise ImageChannelCountException('thresh', 1, subject.shape[2])
    thresh = params.thresh
    maxval = 255
    fn = cv.THRESH_BINARY if not params.invert else cv.THRESH_BINARY_INV
    return cv.threshold(subject, thresh, maxval, fn)[1]
    # for (f_name, f_to_execute) in thresh_funcs.items():
    #     if f_name.lower() == params.f_thresh.lower():
    #         return cv.threshold(subject, thresh, maxval, f_to_execute)[1]
    # raise ParamException('thresh', 'thresholding func', params.f_thresh)

def processing_action_adaptivethresh(subject, params):
    if len(subject.shape) >= 3 and subject.shape[2] != 1:
        raise ImageChannelCountException('adaptive thresh', 1, subject.shape[2])
    maxval = params.maxval
    block_size = params.block_size
    bias = params.bias
    f_adaptive = None
    f_threshold = None
    for (f_name, f_adaptive) in adaptive_thresh_funcs.items():
        if f_name.lower() == params.f_adapt.lower():
            break
    else:
        raise ParamException('adaptive thresh', 'adaptive func', params.f_adapt)
    f_threshold = cv.THRESH_BINARY_INV if params.inverted else cv.THRESH_BINARY
    return cv.adaptiveThreshold(subject, maxval, f_adaptive,
                                f_threshold, block_size, bias)

def processing_action_morph(subject, params):
    if len(subject.shape) >= 3 and subject.shape[2] != 1:
        raise ImageChannelCountException('morph', 1, subject.shape[2])
    kernel_size = params.kernel_size
    f_morph = None
    shape = None
    for (f_name, f_morph) in morph_operations.items():
        if f_name.lower() == params.f_morph.lower():
            break
    else:
        raise ParamException('morph', 'morph func', params.f_adapt)
    for (f_name, shape) in morph_shapes.items():
        if f_name.lower() == params.shape.lower():
            break
    else:
        raise ParamException('morph', 'shape', params.f_thresh)
    kernel = cv.getStructuringElement(shape, (kernel_size, kernel_size))
    return cv.morphologyEx(subject, f_morph, kernel)

def processing_action_watershed_old(subject, params):
    thr = subject
    img = np.zeros((thr.shape[0], thr.shape[1], 3), dtype=np.uint8)

    kernel_size = params.kernel_size
    shape = morph_shapes[params.shape]
    kernel = cv.getStructuringElement(shape, (kernel_size, kernel_size))
    bg = cv.morphologyEx(thr, cv.MORPH_DILATE, kernel)

    # TODO some hard params
    dist = params.dist
    fg = cv.distanceTransform(thr, cv.DIST_L2, 5)
    _, fg = cv.threshold(fg, (dist / 100) * fg.max(), 255, 0)
    fg = np.uint8(fg)

    unknown = cv.subtract(bg, fg)

    _, markers = cv.connectedComponents(fg)
    markers = markers + 1
    markers[unknown == 255] = 0

    markers = cv.watershed(img, markers)
    markers[markers == -1] = 0
    colors = np.uint8(markers * 180 / markers.max())
    img[:, :, 0] = colors
    img[:, :, 1] = 255
    img[:, :, 2] = 128 * (markers > 1) + 127 * (markers > 0)
    return img

def processing_action_label(subject, params):
    connectivity = 2 if params.diag else 1
    markers = label(subject, connectivity=connectivity)
    return markers

def processing_action_watershed(subject, params):
    fg = params._fg_image
    bg = params._bg_image

    unknown = cv.subtract(bg, fg)

    _, markers = cv.connectedComponents(fg)
    markers = markers + 1
    markers[unknown == 255] = 0

    markers = cv.watershed(subject, markers)
    markers[markers == -1] = 0
    return markers

def processing_action_geotrans(subject, params):
    # done using affine
    r = params.r * np.pi / 180
    c = params.c / 100
    h = subject.shape[0]
    w = subject.shape[1]
    r_center = np.float32([[params.r_x * w / 100], [params.r_y * h / 100]])
    c_center = np.float32([[params.c_x * w / 100], [params.c_y * h / 100]])
    t_vector = np.float32([[params.t_x * w / 100], [params.t_y * h / 100]])

    r_matrix = np.float32([[math.cos(r), -math.sin(r)],
                           [math.sin(r),  math.cos(r)]])

    src = np.float32([[0, 0], [0, 1], [1, 0]]).transpose()
    dst = src.copy()

    dst -= r_center ; dst = np.matmul(r_matrix, dst) ; dst += r_center
    dst += t_vector
    dst -= c_center ; dst *= c ; dst += c_center

    transform = cv.getAffineTransform(src.transpose(), dst.transpose())

    return cv.warpAffine(subject, transform, (w, h))

def processing_action_inttrans(subject, params):
    # Euclidean modulo makes me so happy
    rot = params.rot % 4
    h_flip = params.h_flip
    v_flip = params.v_flip
    subject = np.rot90(subject, rot)
    if h_flip:
        subject = np.fliplr(subject)
    if v_flip:
        subject = np.flipud(subject)
    return subject

def processing_action_crop(subject, params):
    left = params.left * subject.shape[1] // 100
    right = (100 - params.right) * subject.shape[1] // 100
    top = params.top * subject.shape[0] // 100
    bottom = (100 - params.bottom) * subject.shape[0] // 100
    return subject[top:bottom, left:right, :]

def processing_action_houghlines(subject, params):
    rho_prec = params.rho_prec
    theta_prec = params.theta_prec
    votes = params.votes
    min_length = params.min_length
    max_gap = params.max_gap
    dst = np.uint8(np.zeros(subject.shape))
    if params.segments:
        lines = cv.HoughLinesP(subject, rho_prec, np.pi/theta_prec, votes,
                               min_length, max_gap)
        for line in lines:
            x1, y1, x2, y2 = line[0]
            cv.line(dst, (x1, y1), (x2, y2), (255), 2)
    else:
        lines = cv.HoughLines(subject, rho_prec, np.pi/theta_prec, votes)
        for line in lines:
            rho, theta = line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a*rho
            y0 = b*rho
            x1 = int(x0 + 1000*(-b))
            y1 = int(y0 + 1000*(a))
            x2 = int(x0 - 1000*(-b))
            y2 = int(y0 - 1000*(a))
            cv.line(dst, (x1, y1), (x2, y2), (255), 2)
    return dst

def processing_action_houghcircles(subject, params):
    min_radius = params.min_radius
    max_radius = params.min_radius
    thresh = params.thresh
    votes = params.votes
    min_dist = params.min_dist
    dst = np.uint8(np.zeros(subject.shape))
    circles = cv.HoughCircles(subject, cv.HOUGH_GRADIENT, 1, min_dist,
                              param1=thresh, param2=votes,
                              minRadius=min_radius, maxRadius=max_radius)
    circles = np.uint16(np.around(circles))
    for i in circles[0,:]:
        cv.circle(dst, (i[0], i[1]), i[2], (255), 2)
        cv.circle(dst, (i[0], i[1]), 2, (255), 3)
    return dst

def processing_action_blur(subject, params):
    kernel_size = params.kernel_size
    for (f_name, f_to_execute) in blurs.items():
        if f_name.lower() == params.f_blur.lower():
            return f_to_execute(subject, kernel_size)
    raise ParamException('thresh', 'thresholding func', params.f_thresh)

def processing_action_frequency_pass_filter(subject, params):
    if params.metric == 'Euclidean' or params.metric == 0:
        dists = (np.arange(subject.shape[0])[:, None] - subject.shape[0] // 2) ** 2 \
              + (np.arange(subject.shape[1])[None, :] - subject.shape[1] // 2) ** 2
        mask = (dists >= (params.low ** 2)) * (dists <= (params.high ** 2))
    if params.metric == 'Taxicab' or params.metric == 1:
        dists = np.abs(np.arange(subject.shape[0])[:, None] - subject.shape[0] // 2) \
              + np.abs(np.arange(subject.shape[1])[None, :] - subject.shape[1] // 2)
        mask = (dists >= params.low) * (dists <= params.high)
    if params.metric == 'Max' or params.metric == 2:
        dists = np.maximum(
            np.abs(np.arange(subject.shape[0])[:, None] - subject.shape[0] // 2),
            np.abs(np.arange(subject.shape[1])[None, :] - subject.shape[1] // 2)
        )
        mask = (dists >= params.low) * (dists <= params.high)
    ft = np.fft.fftshift(np.fft.fft2(subject.astype(np.float32) / 127.5 - 1)) * mask
    rec = np.fft.ifft2(np.fft.ifftshift(ft)).astype(np.float32)
    if params.rescale:
        factor = np.abs(rec).max()
        if factor != 0:
            rec /= factor
    return (rec * 127.5 + 127.5).round().astype(np.uint8)

def processing_action_frequency_homomorphic_filter(subject, params):
    if params.metric == 'Euclidean' or params.metric == 0:
        dists = (np.arange(subject.shape[0])[:, None] - subject.shape[0] // 2) ** 2 \
              + (np.arange(subject.shape[1])[None, :] - subject.shape[1] // 2) ** 2
        dists = np.sqrt(dists)
    if params.metric == 'Taxicab' or params.metric == 1:
        dists = np.abs(np.arange(subject.shape[0])[:, None] - subject.shape[0] // 2) \
              + np.abs(np.arange(subject.shape[1])[None, :] - subject.shape[1] // 2)
    if params.metric == 'Max' or params.metric == 2:
        dists = np.maximum(
            np.abs(np.arange(subject.shape[0])[:, None] - subject.shape[0] // 2),
            np.abs(np.arange(subject.shape[1])[None, :] - subject.shape[1] // 2)
        )
    lm = params.low_multi / 100
    hm = params.high_multi / 100
    x = (dists - params.low) / (params.high - params.low)
    y = lm + x * x * (3 - 2 * x) * (hm - lm)
    y[x < 0] = lm
    y[x > 1] = hm
    ft = np.fft.fftshift(np.fft.fft2(subject.astype(np.float32) / 127.5 - 1)) * y
    rec = np.fft.ifft2(np.fft.ifftshift(ft)).astype(np.float32)
    if params.rescale:
        factor = np.abs(rec).max()
        if factor != 0:
            rec /= factor
    return (rec * 127.5 + 127.5).round().astype(np.uint8)

def processing_action_frequency_adaptivethresh(subject, params):
    if len(subject.shape) >= 3 and subject.shape[2] != 1:
        raise ImageChannelCountException('frequency domain adaptive thresh', 1, subject.shape[2])
    block_size = params.block_size
    bias = params.bias
    f_adaptive = None
    f_threshold = None
    for (f_name, f_adaptive) in adaptive_thresh_funcs.items():
        if f_name.lower() == params.f_adapt.lower():
            break
    else:
        raise ParamException('frequency domain adaptive thresh', 'adaptive func', params.f_adapt)
    f_threshold = cv.THRESH_BINARY_INV if params.inverted else cv.THRESH_BINARY
    ft = np.fft.fftshift(np.fft.fft2(subject.astype(np.float32) / 127.5 - 1))
    lft = np.log(np.abs(ft))
    lft += lft.min()
    lft *= 255 / lft.max()
    mask = cv.adaptiveThreshold(lft.astype(np.uint8), 255, f_adaptive, f_threshold, block_size, bias)
    ft *= mask == 0
    rec = np.fft.ifft2(np.fft.ifftshift(ft)).astype(np.float32)
    return (rec * 127.5 + 127.5).round().astype(np.uint8)

def processing_action_harris_corner_detection(subject, params):
    img = np.float32(subject)
    dst = np.zeros(subject.shape, dtype=np.uint8)
    cornerness = cv.cornerHarris(img, params.block_size, params.kernel_size, params.k * 0.001)
    dst[cornerness > params.thresh * 0.001 * cornerness.max()] = 255
    return dst

def processing_action_shi_tomasi_detection(subject, params):
    img = subject
    dst = np.zeros(subject.shape, dtype=np.uint8)
    corners = cv.goodFeaturesToTrack(img, params.n, params.quality * 0.001, params.min_dist)
    corners = np.int0(corners)
    for i in corners:
        x, y = i.ravel()
        cv.circle(dst, (x, y), 3, 255, -1)
    return dst

def processing_action_sift(subject, params):
    img = subject
    sift = cv.SIFT_create()
    kp, des = sift.detectAndCompute(img, None)
    return {'keypoints': kp, 'descriptors': des}

def processing_action_surf(subject, params):
    img = subject
    surf = cv.xfeatures2d.SURF_create(params.thresh)
    if params.usurf:
        surf.setUpright(True)
    kp, des = surf.detectAndCompute(img, None)
    return {'keypoints': kp, 'descriptors': des}

def processing_action_fast(subject, params):
    img = subject
    fast = cv.FastFeatureDetector_create()
    if not params.nonmax_supp:
        fast.setNonmaxSuppression(0)
    kp, des = fast.detectAndCompute(img, None)
    return {'keypoints': kp, 'descriptors': des}

def processing_action_star_brief(subject, params):
    img = subject
    star = cv.xfeatures2d.StarDetector_create()
    brief = cv.xfeatures2d.BriefDescriptorExtractor_create()
    kp = star.detect(img,None)
    kp, des = brief.compute(img, kp)
    return {'keypoints': kp, 'descriptors': des}

def processing_action_orb(subject, params):
    img = subject
    orb = cv.ORB_create()
    kp, des = orb.detectAndCompute(img, None)
    return {'keypoints': kp, 'descriptors': des}

def processing_action_binoverlay(subject, params):
    dst_color = np.array([params.red, params.green, params.blue])
    dst_image = np.array(params._dst_image)
    mask = subject.astype(bool)
    dst_image[mask] = dst_color
    return dst_image

def processing_action_kpoverlay(subject, params):
    color = (params.red, params.green, params.blue)
    dst_image = np.array(params._dst_image)
    kp = subject['keypoints']
    dst_image = cv.drawKeypoints(dst_image, kp, None, color=color, flags=cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    return dst_image

def processing_action_bfmatch(subject, params):
    des_image = subject['descriptors']
    kp_image = subject['keypoints']
    des_query = params._query_dat['descriptors']
    kp_query = params._query_dat['keypoints']

    if params.k != 1 and params.cross_check:
        raise Exception('Method "Cross check" requires k = 1')
    if params.k != 2 and params.ratio_test:
        raise Exception('Method "Ratio test" requires k = 2')

    ratio = params.ratio / 100
    key = 'match' if params.k == 1 or params.ratio_test else 'knn_match'
    norm = bf_norms[params.norm]
    bf = cv.BFMatcher(norm, crossCheck=params.cross_check)
    if params.k == 1:
        match = bf.match(des_query.astype(np.uint8), des_image.astype(np.uint8))
    else:
        match = bf.knnMatch(des_query.astype(np.uint8), des_image.astype(np.uint8), k=params.k)
        if params.ratio_test:
            match = [i for i, j in match if i.distance < ratio * j.distance]
    return {key: match, 'query_kp': kp_query, 'image_kp': kp_image}

# TODO reduce code duplication (but don't unify processes)
def processing_action_flann_kdtree_match(subject, params):
    des_image = subject['descriptors']
    kp_image = subject['keypoints']
    des_query = params._query_dat['descriptors']
    kp_query = params._query_dat['keypoints']

    if params.k != 2 and params.ratio_test:
        raise Exception('Method "Ratio test" requires k = 2')

    ratio = params.ratio / 100
    key = 'knn_match'
    # key = 'match' if params.k == 1 or params.ratio_test else 'knn_match'
    index = 1
    index_kwargs = {
        'algorithm': index,
        'trees': params.trees,
    }
    search_kwargs = {
        'checks': params.checks,
    }
    flann = cv.FlannBasedMatcher(index_kwargs, search_kwargs)
    match = flann.knnMatch(des_query.astype(np.float32), des_image.astype(np.float32), k=params.k)
    if params.ratio_test:
        match = [(i,) for i, j in match if i.distance < ratio * j.distance]
    return {key: match, 'query_kp': kp_query, 'image_kp': kp_image}

def processing_action_flann_lsh_match(subject, params):
    des_image = subject['descriptors']
    kp_image = subject['keypoints']
    des_query = params._query_dat['descriptors']
    kp_query = params._query_dat['keypoints']

    if params.k != 2 and params.ratio_test:
        raise Exception('Method "Ratio test" requires k = 2')

    ratio = params.ratio / 100
    key = 'knn_match'
    # key = 'match' if params.k == 1 or params.ratio_test else 'knn_match'
    index = 6
    index_kwargs = {
        'algorithm': index,
        'table_number': params.table_number,
        'key_size': params.key_size,
        'multi_probe_level': params.multi_probe_level,
    }
    search_kwargs = {
        'checks': params.checks,
    }
    flann = cv.FlannBasedMatcher(index_kwargs, search_kwargs)
    match = flann.knnMatch(des_query.astype(np.uint8), des_image.astype(np.uint8), k=params.k)
    if params.ratio_test:
        match = [i for i, j in match if i.distance < ratio * j.distance]
    return {key: match, 'query_kp': kp_query, 'image_kp': kp_image}

def processing_action_homography(subject, params):
    image_kp = subject['image_kp']
    query_kp = subject['query_kp']
    match = subject['match'] if 'match' in subject else [i[0] for i in subject['knn_match']]

    query_pts = np.float32([query_kp[i.queryIdx].pt for i in match]).reshape(-1, 1, 2)
    image_pts = np.float32([image_kp[i.trainIdx].pt for i in match]).reshape(-1, 1, 2)
    method = homography_methods[params.method]
    M, mask = cv.findHomography(query_pts, image_pts, method, params.reproj_err, None, 2000, 0.995)
    # mask = [[i, i] for i in mask_raw.ravel().tolist()]
    return subject | {'homography': M, 'homography_mask': mask}

def processing_action_draw_match(subject, params):
    image_kp = subject['image_kp']
    image_img = params._image_img
    query_kp = subject['query_kp']
    query_img = params._query_img
    matches = subject['match'] if 'match' in subject else subject['knn_match']
    no_single = cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS
    mask = None

    if 'homography' in subject:
        M = subject['homography']
        mask = subject['homography_mask']
        if params.do_limit:
            if 'match' in subject:
                mask_tmp = sorted(enumerate(mask), key=lambda x: matches[x[0]].distance)[:params.limit]
            else:
                mask_tmp = sorted(enumerate(mask), key=lambda x: matches[x[0]][0].distance)[:params.limit]
            mask = np.stack([i[1] for i in mask_tmp], axis=0)
        h, w = query_img.shape[:2]
        src = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
        dst = cv.perspectiveTransform(src, M)
        image_img = cv.polylines(image_img, [np.int32(dst)], True, 255, 3, cv.LINE_AA)

    if 'match' in subject:
        if params.do_limit:
            matches = sorted(matches, key=lambda x: x.distance)[:params.limit]
        img = cv.drawMatches(query_img, query_kp, image_img, image_kp, matches, None, flags=no_single, matchesMask=mask)
    else:
        if params.do_limit:
            matches = sorted(matches, key=lambda x: x[0].distance)[:params.limit]
        img = cv.drawMatchesKnn(query_img, query_kp, image_img, image_kp, matches, None, flags=no_single, matchesMask=mask)
    return img

def processing_action_equalize(subject, params):
    if params.clahe:
        tile_size = params.tile_size
        clip_limit = params.clip_limit
        clahe = cv.createCLAHE(clipLimit=clip_limit, tileGridSize=(tile_size, tile_size))
        return clahe.apply(subject)
    else:
        return cv.equalizeHist(subject)

def processing_action_canny(subject, params):
    return cv.Canny(subject, params.lower, params.upper)

def processing_action_sobel(subject, params):
    im = cv.Sobel(subject, cv.CV_16S, params.dx, params.dy, ksize=params.kernel_size)
    return np.uint8(np.absolute(im))

def processing_action_laplace(subject, params):
    im = cv.Laplacian(subject, cv.CV_16S, ksize=params.kernel_size)
    return np.uint8(np.absolute(im))

def processing_action_match(subject, params):
    template = params._template
    multiple = params.multiple
    thresh = params.thresh / 100
    dst = np.zeros(subject.shape[:2], dtype=np.uint8)
    f_match = matchers[params.f_match]
    heatmap = cv.matchTemplate(subject, template, f_match)
    inv = f_match in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]
    h, w = template.shape[:2]
    if multiple:
        loc = np.where(heatmap >= thresh)
        for top_left in zip(*loc[::-1]):
            bottom_right = (top_left[0] + w, top_left[1] + h)
            cv.rectangle(dst, top_left, bottom_right, 255, 2)
    else:
        _, _, min_loc, max_loc = cv.minMaxLoc(heatmap)
        top_left = min_loc if inv else max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        cv.rectangle(dst, top_left, bottom_right, 255, 2)
    return dst

def processing_action_match_model(subject, params):
    templates = params._templates
    multiple = params.multiple
    thresh = params.thresh / 100
    dst = np.zeros(subject.shape[:2], dtype=np.uint8)
    f_match = matchers[params.f_match]
    for n, template in enumerate(templates):
        heatmap = cv.matchTemplate(subject, template, f_match)
        inv = f_match in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]
        h, w = template.shape[:2]
        if multiple:
            loc = np.where(heatmap >= thresh)
            for top_left in zip(*loc[::-1]):
                bottom_right = (top_left[0] + w, top_left[1] + h)
                cv.rectangle(dst, top_left, bottom_right, n+1, 2)
        else:
            _, _, min_loc, max_loc = cv.minMaxLoc(heatmap)
            top_left = min_loc if inv else max_loc
            bottom_right = (top_left[0] + w, top_left[1] + h)
            cv.rectangle(dst, top_left, bottom_right, n+1   , 2)
        return dst

def processing_action_detect(subject, params):
    model = params._model
    detector = dlib.get_frontal_face_detector()
    boxes = detector(subject, 1)
    binary = np.zeros(subject.shape[:2])
    for i in boxes:
        shape = model(subject, i)
        for n in range(68):
            x, y = shape.part(n).x, shape.part(n).y
            cv.circle(binary, (x, y), 1, 255, params.r)
    return binary

def processing_action_recognize(subject, params):
    names = params.datasets
    datasets = params._datasets
    data = {'encodings': [], 'names': []}
    binary = np.zeros(subject.shape[:2])
    for dataset in zip(names, datasets):
        for img in dataset[1]:
            locations = face_recognition.face_locations(subject, model=params.method)
            encodings = face_recognition.face_encodings(img, locations)
            for encoding in encodings:
                data['encodings'].append(encoding)
                data['names'].append(dataset[0])
    locations = face_recognition.face_locations(subject, model=params.method)
    encodings = face_recognition.face_encodings(subject, locations)
    names = []
    min_votes = params.min_votes
    for encoding in encodings:
        matches = face_recognition.compare_faces(data['encodings'], encoding)
        votes = [data['names'][n] for (n, i) in enumerate(matches) if i]
        vote_counts = {k: v for k, v in Counter(votes).items() if v >= min_votes}
        names.append(max(vote_counts.keys(), key=lambda key: vote_counts[key], default='No match'))
    for ((y1, x1, y2, x2), name) in zip(locations, names):
    	cv.rectangle(binary, (x1, y1), (x2, y2), 255, 2)
    	y = y1 - 16 if y1 > 32 else y2 + 16
    	cv.putText(binary, name, (x1, y), cv.FONT_HERSHEY_PLAIN, 2.0, 255, 2)
    return binary


"""
InputSource abstract and impls
"""

class InputSource:
    def __init__(self, label: str, dest: str):
        self.label = label
        self.args = None
        self.action = lambda : None
        self.dest = dest
        self.on_update = None
        self._create_widget()
    def connect(self, args, on_update):
        self.args = args
        self.on_update = on_update
        self._store()
    def load_value(self, value):
        raise NotImplementedError("Please Implement this method")
    def save_value(self):
        raise NotImplementedError("Please Implement this method")
    def _store(self):
        setattr(self.args, self.dest, self._get_new_value())
        if self.on_update is not None:
            self.on_update()
    def _create_widget(self):
        raise NotImplementedError("Please Implement this method")
    def _get_new_value(self):
        raise NotImplementedError("Please Implement this method")

class IntSlider(InputSource):
    def __init__(self, label: str, dest: str, lower: int, upper: int,
                 default: int, step: int = 1):
        self.lower = lower
        self.upper = upper
        self.step = step
        self.default = default
        self._spinner = None
        self._slider = None
        super().__init__(label, dest)
    def load_value(self, value):
        self._slider.setValue(int(value))
    def save_value(self):
        return self._slider.value()
    def _create_widget(self):
        widget = QtWidgets.QWidget()
        layout = QtWidgets.QGridLayout()
        widget.setLayout(layout)

        spinner = QtWidgets.QSpinBox()
        spinner.setMinimum(self.lower)
        spinner.setMaximum(self.upper)
        spinner.setValue(self.default)
        spinner.setSingleStep(self.step)
        spinner.valueChanged.connect(self._store_from_spinner)
        self._spinner = spinner
        layout.addWidget(spinner, 0, 0, 1, 1)

        slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        slider.setMinimum(self.lower)
        slider.setMaximum(self.upper)
        slider.setValue(self.default)
        slider.setSingleStep(self.step)
        slider.valueChanged.connect(self._store_from_slider)
        self._slider = slider
        layout.addWidget(slider, 0, 1, 1, 3)

        self.widget = widget
    def _store_from_spinner(self):
        self._slider.blockSignals(True)
        self._slider.setValue(self._spinner.value())
        self._slider.blockSignals(False)
        self._verify_and_store()
    def _store_from_slider(self):
        self._spinner.blockSignals(True)
        self._spinner.setValue(self._slider.value())
        self._spinner.blockSignals(False)
        self._verify_and_store()
    def _verify_and_store(self):
        val = self._get_new_value()
        err = (val - self.default) % self.step
        if err != 0:
            self._slider.setValue(max(val - err, self.lower))
        else:
            self._store()

    def _get_new_value(self):
        return self._slider.value()

class StrSelect(InputSource):
    def __init__(self, label: str, dest: str, options: list[str]):
        self.options = options
        super().__init__(label, dest)
    def load_value(self, value):
        self.widget.setCurrentIndex(int(value))
    def save_value(self):
        return self.widget.currentIndex()
    def _create_widget(self):
        widget = QtWidgets.QComboBox()
        for i in self.options:
            widget.addItem(i)
        widget.currentIndexChanged.connect(self._store)
        self.widget = widget
    def _get_new_value(self):
        return self.widget.currentText()

# TODO Need this done .. comes with a plethora of issues
class TemplateSelect(InputSource):
    def __init__(self, label: str, dest: str, ctrl: AppControl):
        vc = ctrl.get_variable_container()
        vc.hook_template(self._set_options)
        self.ctrl = ctrl
        self.variable_container = vc
        super().__init__(label, dest)
    def load_value(self, value):
        if value not in self.variable_container._template:
            return
        self.select.setCurrentText(value)
    def save_value(self):
        return self.select.currentText()
    def _create_widget(self):
        self.widget = QtWidgets.QWidget()
        ly = QtWidgets.QHBoxLayout()
        self.widget.setLayout(ly)
        select = QtWidgets.QComboBox()
        select.currentIndexChanged.connect(self._store)
        ly.addWidget(select)
        add = QtWidgets.QPushButton('+')
        add.setMaximumWidth(30)
        add.clicked.connect(self.ctrl.ui._action_model_open_template.trigger)
        ly.addWidget(add)
        cfg = QtWidgets.QPushButton('...')
        cfg.setMaximumWidth(30)
        cfg.clicked.connect(self.ctrl.ui._action_model_view_variables.trigger)
        ly.addWidget(cfg)
        self.select = select
        self._set_options(self.variable_container._template, can_signal=False)
    def _set_options(self, items, can_signal=True):
        self.select.blockSignals(True)
        txt = self.select.currentText()
        self.select.clear()
        self.select.addItem('-- Choose template --')
        self.select.addItems(items)
        if txt in items:
            self.select.setCurrentText(txt)
        elif can_signal:
            self._store()
        self.select.blockSignals(False)
    def _get_new_value(self):
        return self.select.currentText()

# TODO Need this done .. comes with a plethora of issues
class ImageSelect(InputSource):
    def __init__(self, label: str, dest: str, ctrl: AppControl):
        vc = ctrl.get_variable_container()
        vc.hook_image(self._set_options)
        self.ctrl = ctrl
        self.variable_container = vc
        super().__init__(label, dest)
    def load_value(self, value):
        if value not in self.variable_container._image and value != '':
            self.variable_container._image[value] = None # Behavior of image select, as it's assigned in process
        self.select.setCurrentText(value)
    def save_value(self):
        return self.select.currentText()
    def _create_widget(self):
        self.widget = QtWidgets.QWidget()
        ly = QtWidgets.QHBoxLayout()
        self.widget.setLayout(ly)
        select = QtWidgets.QComboBox()
        select.currentIndexChanged.connect(self._store)
        ly.addWidget(select)
        add = QtWidgets.QPushButton('+')
        add.setMaximumWidth(30)
        add.clicked.connect(self.ctrl.ui._action_model_add_variable.trigger) # TODO action
        ly.addWidget(add)
        cfg = QtWidgets.QPushButton('...')
        cfg.setMaximumWidth(30)
        cfg.clicked.connect(self.ctrl.ui._action_model_view_variables.trigger)
        ly.addWidget(cfg)
        self.select = select
        self._set_options(self.variable_container._image, can_signal=False)
    def _set_options(self, items, can_signal=True):
        self.select.blockSignals(True)
        txt = self.select.currentText()
        self.select.clear()
        self.select.addItem('-- Choose image --')
        self.select.addItems(items)
        if txt in items:
            self.select.setCurrentText(txt)
        elif can_signal:
            self._store()
        self.select.blockSignals(False)
    def _get_new_value(self):
        return self.select.currentText()

class IntVectorBox(InputSource):
    def __init__(self, label: str, dest: str, default: list[int]):
        self.default = default
        super().__init__(label, dest)
    def load_value(self, value):
        self.widget.setText(value)
    def save_value(self):
        return self.widget.text()
    def _create_widget(self):
        widget = QtWidgets.QLineEdit()
        widget.setValidator(IntListValidator())
        widget.setText(','.join([str(i) for i in self.default]))
        widget.textChanged.connect(self._store)
        self.widget = widget
    def _get_new_value(self):
        return [int(i.strip()) for i in self.widget.text().split(',') if i != '']

class StrVectorBox(InputSource):
    def __init__(self, label: str, dest: str, default: list[str]):
        self.default = default
        super().__init__(label, dest)
    def load_value(self, value):
        self.widget.setText(value)
    def save_value(self):
        return self.widget.text()
    def _create_widget(self):
        widget = QtWidgets.QLineEdit()
        widget.setText(','.join([str(i) for i in self.default]))
        widget.textChanged.connect(self._store)
        self.widget = widget
    def _get_new_value(self):
        return [i.strip() for i in self.widget.text().split(',') if i != '']

class BoolCheckBox(InputSource):
    def __init__(self, label: str, dest: str, default: bool):
        self.default = default
        self.box_label = label
        super().__init__('', dest)
    def load_value(self, value):
        self.widget.setChecked(bool(int(value)))
    def save_value(self):
        return int(self.widget.isChecked())
    def _create_widget(self):
        widget = QtWidgets.QCheckBox(self.box_label, None)
        widget.setChecked(self.default)
        widget.stateChanged.connect(self._store)
        self.widget = widget
    def _get_new_value(self):
        return self.widget.isChecked()

class StrBox(InputSource):
    def __init__(self, label: str, dest: str, default: str):
        self.default = default
        super().__init__(label, dest)
    def load_value(self, value):
        self.widget.setText(value)
    def save_value(self):
        return self.widget.text()
    def _create_widget(self):
        widget = QtWidgets.QLineEdit()
        widget.setText(self.default)
        widget.textChanged.connect(self._store)
        self.widget = widget
    def _get_new_value(self):
        return self.widget.text()

class ChannelPicker(InputSource):
    def __init__(self, label: str, dest: str, width: int):
        self.default = 'gray'
        self.width = width
        super().__init__(label, dest)
    def load_value(self, value):
        self.buttons[value].setChecked(True)
    def save_value(self):
        return self._get_new_value()
    def _create_widget(self):
        widget = QtWidgets.QWidget()
        layout = QtWidgets.QGridLayout()
        buttons = dict()
        widget.setLayout(layout)
        options = [
            'R - RGB', 'G - RGB', 'B - RGB',
            'Y - YCbCr', 'Cb - YCbCr', 'Cr - YCbCr',
            'H - HSV', 'S - HSV', 'V - HSV',
            'gray'
        ]
        for n, i in enumerate(options):
            button = QtWidgets.QRadioButton(i, widget)
            if i == self.default:
                button.setChecked(True)
            button.toggled.connect(self._store)
            buttons[i] = button
            layout.addWidget(button, n // self.width, n % self.width)
        self.widget = widget
        self.buttons = buttons
    def _get_new_value(self):
        return [i for i in self.buttons.values() if i.isChecked()][0].text()

"""
ImageProcess abstract and impls
"""

class ImageProcess:
    def __init__(self, name: str, func, ctrl: AppControl, on_update = None):
        self.name = name
        self.func = func
        self.args = SimpleNamespace()
        self.inputs = []
        self.on_update = on_update
        self.ctrl = ctrl
        self._add_args()
    def _add_args(self):
        raise NotImplementedError("Please Implement this method")
    def _add_arg(self, input_source: InputSource):
        self.inputs.append(input_source)
        input_source.connect(self.args, self.on_update)
    def long_name(self):
        return self.name
    def apply_to_procedure(self, procedure: ProcedureMeta):
        pass
    def display_settings_at(self, layout: QtWidgets.QFormLayout):
        for i in self.inputs:
            label = QtWidgets.QLabel(i.label)
            label.setAlignment(QtCore.Qt.AlignVCenter)
            label.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
            layout.addRow(label, i.widget)
    def type_constraints(self) -> Tuple[str, str]:
        return None
    def assert_type(self, input_type: str) -> str:
        constraints = self.type_constraints()
        if constraints is None:
            return input_type
        if constraints[0] is not None and input_type is not None and not image_types[constraints[0]].can_accept(input_type):
            raise ImageTypeException(self.name, constraints[0], input_type)
        return constraints[1]
    def apply(self, subject):
        return self.func(subject, self.args)
    def load(self, vals: List):
        for n, i in enumerate(self.inputs):
            i.load_value(vals[n])
    def save(self) -> List:
        return [i.save_value() for i in self.inputs]

class NoopProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('-- Choose function --', lambda s, p : s, ctrl, on_update)
    def _add_args(self):
        pass

class ChannelProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('channel', processing_action_channel, ctrl, on_update)
    def long_name(self):
        return self.name + ': ' + self.args.channel
    def type_constraints(self) -> Tuple[str, str]:
        return ('rgb', 'gray')
    def _add_args(self):
        self._add_arg(ChannelPicker('Channel:', 'channel', 3))

# class ColorspaceProcess(ImageProcess):
#     def __init__(self, on_update = None):
#         super().__init__('colorspace', processing_action_colorspace, on_update)
#     def long_name(self):
#         return self.name + ': ' + self.args.f_conv
#     def _add_args(self):
#         self._add_arg(StrSelect('From - To:', 'f_conv', colorspace_common_funcs))

# class AdvancedColorspaceProcess(ImageProcess):
#     def __init__(self, on_update = None):
#         super().__init__('advanced colorspace', processing_action_colorspace, on_update)
#     def long_name(self):
#         return self.name + ': ' + self.args.f_conv
#     def _add_args(self):
#         self._add_arg(StrSelect('From - To:', 'f_conv', colorspace_conversion_funcs))

class InRangeProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('in range', processing_action_inrange, ctrl, on_update)
    def long_name(self):
        return self.name + ': ' + str(self.args.lower) + ' - ' + str(self.args.upper)
    def type_constraints(self) -> Tuple[str, str]:
        return ('rgb', 'rgb')
    def _add_args(self):
        self._add_arg(IntVectorBox('Lower:', 'lower', [0,0,0]))
        self._add_arg(IntVectorBox('Upper:', 'upper', [255,255,255]))

class InRangeOtherwiseProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('in range otherwise', processing_action_inrangeotherwise, ctrl, on_update)
    def long_name(self):
        return self.name + ': ' + str(self.args.lower) + ' - ' + str(self.args.upper)
    def type_constraints(self) -> Tuple[str, str]:
        return ('rgb', 'rgb')
    def _add_args(self):
        self._add_arg(IntVectorBox('Lower:', 'lower', [0,0,0]))
        self._add_arg(IntVectorBox('Upper:', 'upper', [255,255,255]))
        self._add_arg(IntVectorBox('Other:', 'other', [0,0,0]))

class ThresholdProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('threshold', processing_action_thresh, ctrl, on_update)
    # def long_name(self):
    #     return self.name + ': ' + self.args.f_thresh
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'bin')
    def _add_args(self):
        # self._add_arg(StrSelect('Type:', 'f_thresh', thresh_funcs))
        # self._add_arg(IntSlider('Max value:', 'maxval', 0, 255, 255))
        self._add_arg(IntSlider('Threshold:', 'thresh', 0, 255, 127))
        self._add_arg(BoolCheckBox('Inverted', 'invert', False))

class AdaptiveThresholdProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('adaptive threshold', processing_action_adaptivethresh, ctrl, on_update)
    def long_name(self):
        return self.name + ': ' + self.args.f_adapt
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'bin')
    def _add_args(self):
        self._add_arg(StrSelect('Type:', 'f_adapt', adaptive_thresh_funcs))
        self._add_arg(BoolCheckBox('Inverted', 'inverted', False))
        self._add_arg(IntSlider('Max value:', 'maxval', 0, 255, 255))
        self._add_arg(IntSlider('Block size:', 'block_size', 3, 63, 9, 2))
        self._add_arg(IntSlider('Bias:', 'bias', -63, 63, 0))

class MorphProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('morph', processing_action_morph, ctrl, on_update)
    def long_name(self):
        return self.name + ': ' + self.args.f_morph
    def type_constraints(self) -> Tuple[str, str]:
        return ('bin', 'bin')
    def _add_args(self):
        self._add_arg(StrSelect('Type:', 'f_morph', morph_operations))
        self._add_arg(StrSelect('Kernel shape:', 'shape', morph_shapes))
        self._add_arg(IntSlider('Kernel size:', 'kernel_size', 3, 63, 9, 2))

class WatershedProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('watershed', processing_action_watershed, ctrl, on_update)
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._fg_image = procedure.storage[self.args.fg][0]
        self.args._bg_image = procedure.storage[self.args.bg][0]
    def type_constraints(self) -> Tuple[str, str]:
        return ('rgb', 'key')
    def _add_args(self):
        self._add_arg(ImageSelect('Sure fg:', 'fg', self.ctrl))
        self._add_arg(ImageSelect('Sure bg:', 'bg', self.ctrl))

class LabelProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('label', processing_action_label, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('bin', 'key')
    def _add_args(self):
        self._add_arg(StrSelect('Type:', 'f_blur', blurs))
        self._add_arg(BoolCheckBox('Diagonal neighbors:', 'diag', True))

class BlurProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('blur', processing_action_blur, ctrl, on_update)
    def long_name(self):
        return self.name + ': ' + self.args.f_blur
    def _add_args(self):
        self._add_arg(StrSelect('Type:', 'f_blur', blurs))
        self._add_arg(IntSlider('Kernel size:', 'kernel_size', 3, 63, 9, 2))

class EqualizeProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('equalize', processing_action_equalize, ctrl, on_update)
    def long_name(self):
        return self.name + (' (CLAHE)' if self.args.clahe else '')
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'gray')
    def _add_args(self):
        self._add_arg(BoolCheckBox('CLAHE', 'clahe', False))
        self._add_arg(IntSlider('CLAHE tile size:', 'tile_size', 2, 64, 8, 2))
        self._add_arg(IntSlider('CLAHE clip limit:', 'clip_limit', 1, 100, 40))

class CannyEdgeProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('canny edge detection', processing_action_canny, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'bin')
    def _add_args(self):
        self._add_arg(IntSlider('Lower:', 'lower', 0, 255, 63))
        self._add_arg(IntSlider('Upper:', 'upper', 0, 255, 127))

class SobelEdgeProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('sobel edge detection', processing_action_sobel, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'gray')
    def _add_args(self):
        self._add_arg(IntSlider('Kernel size:', 'kernel_size', 1, 7, 3, 2))
        self._add_arg(IntSlider('Order of dx:', 'dx', 0, 7, 1))
        self._add_arg(IntSlider('Order of dy:', 'dy', 0, 7, 1))

class LaplacianEdgeProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('laplacian edge detection', processing_action_laplace, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'gray')
    def _add_args(self):
        self._add_arg(IntSlider('Kernel size:', 'kernel_size', 1, 31, 3, 2))

class GeoTransProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('geometric transform', processing_action_geotrans, ctrl, on_update)
    def _add_args(self):
        self._add_arg(IntSlider('Rotation degrees:', 'r', 0, 359, 0))
        self._add_arg(IntSlider('Rotation center X %:', 'r_x', 0, 100, 50))
        self._add_arg(IntSlider('Rotation center Y %:', 'r_y', 0, 100, 50))
        self._add_arg(IntSlider('Translation X %:', 't_x', 0, 100, 0))
        self._add_arg(IntSlider('Translation Y %:', 't_y', 0, 100, 0))
        self._add_arg(IntSlider('Scale %:', 'c', 0, 200, 100))
        self._add_arg(IntSlider('Scale center X %:', 'c_x', 0, 100, 50))
        self._add_arg(IntSlider('Scale center Y %:', 'c_y', 0, 100, 50))

class IntTransProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('integer transform', processing_action_inttrans, ctrl, on_update)
    def _add_args(self):
        self._add_arg(IntSlider('Rotations:', 'rot', -2, 2, 0))
        self._add_arg(BoolCheckBox('Flip horizontally', 'h_flip', False))
        self._add_arg(BoolCheckBox('Flip vertically', 'v_flip', False))

class CropProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('crop', processing_action_crop, ctrl, on_update)
    def long_name(self):
        return (self.name + str((self.args.left, self.args.top))
            + ' - ' + str((100 - self.args.right, 100 - self.args.bottom)))
    def _add_args(self):
        self._add_arg(IntSlider('Left %:', 'left', 0, 100, 0))
        self._add_arg(IntSlider('Right %:', 'right', 0, 100, 0))
        self._add_arg(IntSlider('Top %:', 'top', 0, 100, 0))
        self._add_arg(IntSlider('Bottom %:', 'bottom', 0, 100, 0))

class HoughLinesProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('hough line transform', processing_action_houghlines, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('bin', 'bin')
    def _add_args(self):
        self._add_arg(IntSlider('Rho precision:', 'rho_prec', 1, 64, 1))
        self._add_arg(IntSlider('Theta precision:', 'theta_prec', 18, 360, 180))
        self._add_arg(IntSlider('Min votes:', 'votes', 4, 512, 128))
        self._add_arg(BoolCheckBox('Segments', 'segments', False))
        self._add_arg(IntSlider('Min length:', 'min_length', 4, 512, 128))
        self._add_arg(IntSlider('Max gap:', 'max_gap', 4, 512, 128))

class HoughCirclesProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('hough circle transform', processing_action_houghcircles, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('bin', 'bin')
    def _add_args(self):
        self._add_arg(IntSlider('Min radius:', 'min_radius', 0, 512, 32))
        self._add_arg(IntSlider('Max radius:', 'max_radius', 0, 512, 128))
        self._add_arg(IntSlider('Min votes:', 'votes', 4, 512, 256))
        self._add_arg(IntSlider('Min spacing:', 'min_dist', 4, 512, 64))
        self._add_arg(IntSlider('Threshold:', 'thresh', 0, 255, 127))

class FrequencyPassFilterProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('frequency pass filter', processing_action_frequency_pass_filter, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'gray')
    def _add_args(self):
        self._add_arg(IntSlider('Lowest frequency:', 'low', 0, 2000, 0))
        self._add_arg(IntSlider('Highest frequency:', 'high', 0, 2000, 2000)) # TODO dynamic range
        self._add_arg(BoolCheckBox('Rescale output', 'rescale', True))
        self._add_arg(StrSelect('Metric', 'metric', ['Euclidean', 'Taxicab', 'Max']))
        # self._add_arg(StrSelect('Metric', 'metric', {'Euclidean': 0, 'Taxicab': 1, 'Max': 2}))
        # self._add_arg(StrSelect('Window (WIP', 'window', ['Flat']))

class FrequencyHomomorphicFilterProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('frequency domain homomorphic filter', processing_action_frequency_homomorphic_filter, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'gray')
    def _add_args(self):
        self._add_arg(IntSlider('Transition start:', 'low', 0, 2000, 50))
        self._add_arg(IntSlider('Transition end:', 'high', 0, 2000, 100)) # TODO dynamic range
        self._add_arg(IntSlider('Multiplier below %:', 'low_multi', 0, 200, 50))
        self._add_arg(IntSlider('Multiplier above %:', 'high_multi', 0, 200, 100)) # TODO dynamic range
        self._add_arg(BoolCheckBox('Rescale output', 'rescale', True))
        self._add_arg(StrSelect('Metric', 'metric', ['Euclidean', 'Taxicab', 'Max']))
        # self._add_arg(StrSelect('Metric', 'metric', {'Euclidean': 0, 'Taxicab': 1, 'Max': 2}))
        # self._add_arg(StrSelect('Window (WIP', 'window', ['Flat']))

class FrequencyDomainAdaptiveThresholdProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('frequency domain adaptive threshold', processing_action_frequency_adaptivethresh, ctrl, on_update)
    def long_name(self):
        return self.name# + ': ' + self.args.f_adapt
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'gray')
    def _add_args(self):
        self._add_arg(StrSelect('Type:', 'f_adapt', adaptive_thresh_funcs))
        self._add_arg(BoolCheckBox('Inverted', 'inverted', False))
        self._add_arg(IntSlider('Block size:', 'block_size', 3, 63, 9, 2))
        self._add_arg(IntSlider('Bias:', 'bias', -63, 63, 0))
        # self._add_arg(IntSlider('Except frequencies up to:', 'except', 0, 2000, 0))
        self._add_arg(BoolCheckBox('Rescale output', 'rescale', True))

class HarrisCornerDetectionProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('Harris corner detection', processing_action_harris_corner_detection, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'bin')
    def _add_args(self):
        self._add_arg(IntSlider('Block size:', 'block_size', 1, 8, 2))
        self._add_arg(IntSlider('Sobel kernel size:', 'kernel_size', 1, 7, 3, 2))
        self._add_arg(IntSlider('"k" (x1000):', 'k', 1, 100, 40))
        self._add_arg(IntSlider('Threshold (x1000):', 'thresh', 1, 100, 10))

class ShiThomasDetectionProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('Shi Thomas feature detection', processing_action_shi_tomasi_detection, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'bin')
    def _add_args(self):
        self._add_arg(IntSlider('N corners to find:', 'n', 1, 100, 25))
        self._add_arg(IntSlider('Minimum separation:', 'min_dist', 1, 100, 10))
        self._add_arg(IntSlider('Minimum quality:', 'quality', 1, 100, 10))

class SIFTFeatureDetectionProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('SIFT feature detection', processing_action_sift, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'dat')
    def _add_args(self):
        pass

class SURFFeatureDetectionProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('SURF feature detection', processing_action_surf, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'dat')
    def _add_args(self):
        self._add_arg(IntSlider('Threshold:', 'thresh', 1, 2500, 500))
        self._add_arg(BoolCheckBox('U-SURF', 'usurf', False))

class FASTFeatureDetectionProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('FAST feature detection', processing_action_fast, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'dat')
    def _add_args(self):
        self._add_arg(BoolCheckBox('non-max suppression', 'nonmax_supp', True))

class STARFeatureDetectionProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('STAR+BRIEF feature detection', processing_action_star_brief, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'dat')
    def _add_args(self):
        pass

class ORBFeatureDetectionProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('ORB feature detection', processing_action_orb, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'dat')
    def _add_args(self):
        pass

class MatchProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('match template', processing_action_match, ctrl, on_update)
    def long_name(self):
        return self.name + ' ' + self.args.template
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._template = procedure.models.templates[self.args.template][0]
    def type_constraints(self) -> Tuple[str, str]:
        return ('rgb', 'bin')
    def _add_args(self):
        self._add_arg(StrSelect('Method:', 'f_match', matchers))
        self._add_arg(TemplateSelect('Template:', 'template', self.ctrl))
        self._add_arg(BoolCheckBox('Allow multiple', 'multiple', False))
        self._add_arg(IntSlider('Threshold:', 'thresh', 1, 100, 80))

class DetectProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('detect', processing_action_detect, ctrl, on_update)
    def long_name(self):
        return self.name + ' ' + self.args.model
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._model = procedure.models.recognition_model[self.args.model]
    def type_constraints(self) -> Tuple[str, str]:
        return ('gray', 'bin')
    def _add_args(self):
        self._add_arg(StrBox('Detector:', 'model', 'MODEL'))
        self._add_arg(IntSlider('Highlight radius:', 'r', 1, 100, 1))

class RecognizeProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('recognize', processing_action_recognize, ctrl, on_update)
    def long_name(self):
        return self.name + ' ' + str(self.args.datasets)
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._datasets = [procedure.models.learning_data[i] for i in self.args.datasets]
    def type_constraints(self) -> Tuple[str, str]:
        return ('rgb', 'bin')
    def _add_args(self):
        self._add_arg(StrVectorBox('Datasets:', 'datasets', ['SET_1', 'SET_2']))
        self._add_arg(StrSelect('Method:', 'method', ['hog', 'cnn']))
        self._add_arg(IntSlider('Min votes:', 'min_votes', 1, 100, 1))

class BinOverlayProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('binary overlay', processing_action_binoverlay, ctrl, on_update)
    def long_name(self):
        return self.name + ' onto: ' + self.args.dst
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._dst_image = procedure.storage[self.args.dst][0]
    def type_constraints(self) -> Tuple[str, str]:
        return ('bin', 'rgb')
    def _add_args(self):
        self._add_arg(ImageSelect('Destination:', 'dst', self.ctrl))
        self._add_arg(IntSlider('Red:', 'red', 0, 255, 255))
        self._add_arg(IntSlider('Green:', 'green', 0, 255, 0))
        self._add_arg(IntSlider('Blue:', 'blue', 0, 255, 0))

class BfMatchProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('brute force feature match', processing_action_bfmatch, ctrl, on_update)
    def long_name(self):
        return self.name + ' ' + self.args.query_dat
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._query_dat = procedure.models.templates[self.args.query_dat][0]
    def type_constraints(self) -> Tuple[str, str]:
        return ('dat', 'dat')
    def _add_args(self):
        self._add_arg(TemplateSelect('Query keypoints template:', 'query_dat', self.ctrl))
        self._add_arg(StrSelect('Norm:', 'norm', bf_norms))
        self._add_arg(IntSlider('k:', 'k', 1, 10, 1))
        self._add_arg(BoolCheckBox('Cross check:', 'cross_check', False))
        self._add_arg(BoolCheckBox('Ratio test:', 'ratio_test', False))
        self._add_arg(IntSlider('Ratio (%):', 'ratio', 1, 100, 75))

class FlannKdtreeMatchProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('FLANN kdtree feature match', processing_action_flann_kdtree_match, ctrl, on_update)
    def long_name(self):
        return self.name + ' ' + self.args.query_dat
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._query_dat = procedure.models.templates[self.args.query_dat][0]
    def type_constraints(self) -> Tuple[str, str]:
        return ('dat', 'dat')
    def _add_args(self):
        self._add_arg(TemplateSelect('Query keypoints template:', 'query_dat', self.ctrl))
        self._add_arg(IntSlider('k:', 'k', 1, 10, 1))
        self._add_arg(BoolCheckBox('Ratio test:', 'ratio_test', False))
        self._add_arg(IntSlider('Ratio (%):', 'ratio', 1, 100, 75))
        self._add_arg(IntSlider('Trees:', 'trees', 1, 20, 5))
        self._add_arg(IntSlider('Checks:', 'checks', 1, 200, 50))

class FlannLshMatchProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('FLANN LSH feature match', processing_action_flann_lsh_match, ctrl, on_update)
    def long_name(self):
        return self.name + ' ' + self.args.query_dat
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._query_dat = procedure.models.templates[self.args.query_dat][0]
    def type_constraints(self) -> Tuple[str, str]:
        return ('dat', 'dat')
    def _add_args(self):
        self._add_arg(TemplateSelect('Query keypoints template:', 'query_dat', self.ctrl))
        self._add_arg(IntSlider('k:', 'k', 1, 10, 1))
        self._add_arg(BoolCheckBox('Ratio test:', 'ratio_test', False))
        self._add_arg(IntSlider('Ratio (%):', 'ratio', 1, 100, 75))
        self._add_arg(IntSlider('Table number:', 'table_number', 1, 50, 12))
        self._add_arg(IntSlider('Key size:', 'key_size', 1, 50, 10))
        self._add_arg(IntSlider('Multi probe level:', 'multi_probe_level', 1, 5, 2))
        self._add_arg(IntSlider('Checks:', 'checks', 1, 200, 50))

class HomographyProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('find homography', processing_action_homography, ctrl, on_update)
    def type_constraints(self) -> Tuple[str, str]:
        return ('dat', 'dat')
    def _add_args(self):
        self._add_arg(StrSelect('Method:', 'method', homography_methods))
        self._add_arg(IntSlider('Max reprojection error:', 'reproj_err', 1, 10, 3))

class DrawMatchProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('draw match', processing_action_draw_match, ctrl, on_update)
    def long_name(self):
        return self.name + ' ' + self.args.query_img + ' in ' + self.args.image_img
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._query_img = procedure.models.templates[self.args.query_img][0]
        self.args._image_img = procedure.storage[self.args.image_img][0]
    def type_constraints(self) -> Tuple[str, str]:
        return ('dat', 'rgb')
    def _add_args(self):
        self._add_arg(TemplateSelect('Query template:', 'query_img', self.ctrl))
        self._add_arg(ImageSelect('Image:', 'image_img', self.ctrl))
        self._add_arg(BoolCheckBox('Limit displayed matches:', 'do_limit', False))
        self._add_arg(IntSlider('Limit:', 'limit', 1, 100, 10))

class KpOverlayProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('keypoint overlay', processing_action_kpoverlay, ctrl, on_update)
    def long_name(self):
        return self.name + ' onto: ' + self.args.dst
    def apply_to_procedure(self, procedure: ProcedureMeta):
        self.args._dst_image = procedure.storage[self.args.dst][0]
    def type_constraints(self) -> Tuple[str, str]:
        return ('dat', 'rgb')
    def _add_args(self):
        self._add_arg(ImageSelect('Destination:', 'dst', self.ctrl))
        # self._add_arg(IntVectorBox('Color:', 'dst_color', [255, 0, 0]))
        self._add_arg(IntSlider('Red:', 'red', 0, 255, 255))
        self._add_arg(IntSlider('Green:', 'green', 0, 255, 0))
        self._add_arg(IntSlider('Blue:', 'blue', 0, 255, 0))

class StoreMetaProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('-- store', lambda s, p : s, ctrl, on_update)
    def long_name(self):
        return self.name + ' in ' + self.args.dst
    def apply_to_procedure(self, procedure: ProcedureMeta):
        procedure.storage[self.args.dst] = (procedure.image, procedure.type)
    def _add_args(self):
        self._add_arg(ImageSelect('Destination:', 'dst', self.ctrl))

class LoadMetaProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('-- load', lambda s, p : s, ctrl, on_update)
    def long_name(self):
        return self.name + ' from ' + self.args.src
    def apply_to_procedure(self, procedure: ProcedureMeta):
        procedure.image = procedure.storage[self.args.src][0]
        procedure.type = procedure.storage[self.args.src][1]
    def type_constraints(self) -> Tuple[str, str]:
        return (None, None)
    def _add_args(self):
        self._add_arg(ImageSelect('Source:', 'src', self.ctrl))

class StoreLoadMetaProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('-- store and load', lambda s, p : s, ctrl, on_update)
    def long_name(self):
        return '-- store in ' + self.args.dst + ' and load from ' + self.args.src
    def apply_to_procedure(self, procedure: ProcedureMeta):
        procedure.storage[self.args.dst] = (procedure.image, procedure.type)
        procedure.image = procedure.storage[self.args.src][0]
        procedure.type = procedure.storage[self.args.src][1]
    def type_constraints(self) -> Tuple[str, str]:
        return (None, None)
    def _add_args(self):
        self._add_arg(ImageSelect('Destination:', 'dst', self.ctrl))
        self._add_arg(ImageSelect('Source:', 'src', self.ctrl))

class CommentMetaProcess(ImageProcess):
    def __init__(self, ctrl, on_update = None):
        super().__init__('-- comment', lambda s, p : s, ctrl, on_update)
    def long_name(self):
        return '-- # ' + self.args.comment
    def _add_args(self):
        self._add_arg(StrBox('Comment:', 'comment', ''))


"""
Exceptions
"""

class NargsException(Exception):
    def __init__(self, ctx: str, expected: int, given: int):
        super().__init__(('Wrong number of params in context %s: '
                          '%d expected, %d given') % (ctx, expected, given))
        self.expected = expected
        self.given = given

class ParamException(Exception):
    def __init__(self, ctx: str, expected: str, given: str):
        super().__init__(('Unknown param in context %s: '
                          '%s expected, %s given') % (ctx, expected, given))
        self.expected = expected
        self.given = given

class ChannelCountException(Exception):
    def __init__(self, ctx: str, expected: int, given: int):
        super().__init__(('Wrong channel count in context %s: Image has '
                          '%d channels, %d given') % (ctx, expected, given))
        self.expected = expected
        self.given = given

class ImageChannelCountException(Exception):
    def __init__(self, ctx: str, expected: int, given: int):
        super().__init__(('Wrong channel count in context %s: Image has '
                          '%d channels, %d expected') % (ctx, given, expected))
        self.expected = expected
        self.given = given

class ImageTypeException(Exception):
    def __init__(self, ctx: str, expected: ImageType, given: ImageType):
        super().__init__(('Wrong image type in context %s: Image is of type '
                          '%s, but %s expected') % (ctx, given, expected))
        self.expected = expected
        self.given = given

"""
Initialization
"""

# colorspace_conversion_funcs = {
#     i: vars(cv)[i] for i in dir(cv) if i.startswith('COLOR_')
# }

# colorspace_common_funcs = {
#     i: colorspace_conversion_funcs[i] for i in [
#         'COLOR_BGR2RGB', 'COLOR_RGB2BGR',
#         'COLOR_BGR2GRAY', 'COLOR_GRAY2BGR',
#         'COLOR_BGR2HSV', 'COLOR_HSV2BGR',
#         'COLOR_BGR2YUV', 'COLOR_YUV2BGR',
#         'COLOR_BGR2YCrCb', 'COLOR_YCrCb2BGR'
#     ]
# }

# TODO some are valid, some are thresh detection algos, some don't work with adaptive
thresh_funcs = {
    i: vars(cv)[i] for i in dir(cv) if i.startswith('THRESH_')
}

adaptive_thresh_funcs = {
    i: vars(cv)[i] for i in dir(cv) if i.startswith('ADAPTIVE_THRESH_')

}
morph_shapes = {
    i: vars(cv)[i] for i in [
        'MORPH_RECT', 'MORPH_ELLIPSE', 'MORPH_CROSS'
    ]
}

morph_operations = {
    i: vars(cv)[i] for i in dir(cv) if i.startswith('MORPH_') and i not in [
        'MORPH_RECT', 'MORPH_ELLIPSE', 'MORPH_CROSS'
    ]
}

morph_operations = dict(sorted(morph_operations.items(), key=lambda item: item[1]))

blurs = {
    'BLUR_MEAN': lambda img, size : cv.blur(img, (size, size)),
    'BLUR_GAUSS': lambda img, size : cv.GaussianBlur(img, (size, size), 0),
    'BLUR_MEDIAN': lambda img, size : cv.medianBlur(img, size),
    # TODO sigma input
    'BLUR_BILATERAL': lambda img, size : cv.bilateralFilter(img, size, 75, 75),
}

matchers = dict(i for i in vars(cv).items() if i[0].startswith('TM_'))

bf_norms = {i: vars(cv)[i] for i in ['NORM_HAMMING', 'NORM_HAMMING2', 'NORM_L1', 'NORM_L2']}

homography_methods = {i: vars(cv)[i] for i in ['RANSAC', 'LMEDS', 'RHO'] if i in dir(cv)} | {'LSQR': 0}

image_types = {
    'rgb': RgbType(),
    'bin': BinType(),
    'gray': GrayType(),
    'key': KeyType(),
    'dat': DatType(),
}

process_tree = {
    'colorspace': {
        'channel': ChannelProcess,
        'equalize': EqualizeProcess,
    },
    'thresholding': {
        'in range': InRangeProcess,
        'in range otherwise': InRangeOtherwiseProcess,
        'threshold': ThresholdProcess,
        'adaptive threshold': AdaptiveThresholdProcess,
    },
    'filtering': {
        'morph': MorphProcess,
        'blur': BlurProcess,
        'frequency pass filter': FrequencyPassFilterProcess,
        'frequency domain homomorphic filter': FrequencyHomomorphicFilterProcess,
    },
    'segmentation': {
        'watershed': WatershedProcess,
        'label': LabelProcess,
    },
    'transform': {
        'geometric transform': GeoTransProcess,
        'integer transform': IntTransProcess,
        'crop': CropProcess,
        'hough line transform': HoughLinesProcess,
        'hough circle transform': HoughCirclesProcess,
    },
    'edge detection': {
        'canny edge detection': CannyEdgeProcess,
        'sobel edge detection': SobelEdgeProcess,
        'laplacian edge detection': LaplacianEdgeProcess,
    },
    'feature detection': {
        'Harris corner detection': HarrisCornerDetectionProcess,
        'Shi Thomas feature detection': ShiThomasDetectionProcess,
        'SIFT feature detection': SIFTFeatureDetectionProcess,
        'SURF feature detection': SURFFeatureDetectionProcess,
        'FAST feature detection': FASTFeatureDetectionProcess,
        'STAR+BRIEF feature detection': STARFeatureDetectionProcess,
        'ORB feature detection': ORBFeatureDetectionProcess,
    },
    'other detection': {
        'match template': MatchProcess,
        'detect': DetectProcess,
        'recognize': RecognizeProcess,
    },
    'feature matching': {
        'brute force feature match': BfMatchProcess,
        'FLANN kdtree feature match': FlannKdtreeMatchProcess,
        'FLANN LSH feature match': FlannLshMatchProcess,
        'find homography': HomographyProcess,
    },
    'rendering results': {
        'binary overlay': BinOverlayProcess,
        'keypoint overlay': KpOverlayProcess,
        'draw match': DrawMatchProcess,
    },
    'meta': {
        '-- store': StoreMetaProcess,
        '-- load': LoadMetaProcess,
        '-- store and load': StoreLoadMetaProcess,
        '-- comment': CommentMetaProcess,
    },
}

processes = {'-- Choose function --': NoopProcess} | {k: v for d in process_tree.values() for k, v in d.items()}

# processes = {
#     '-- Choose function --': NoopProcess,
#     'channel': ChannelProcess,
#     # 'colorspace': ColorspaceProcess,
#     # 'advanced colorspace': AdvancedColorspaceProcess,
#     'in range': InRangeProcess,
#     'in range otherwise': InRangeOtherwiseProcess,
#     'threshold': ThresholdProcess,
#     'adaptive threshold': AdaptiveThresholdProcess,
#     'morph': MorphProcess,
#     'watershed': WatershedProcess,
#     'label': LabelProcess,
#     'blur': BlurProcess,
#     'equalize': EqualizeProcess,
#     'frequency pass filter': FrequencyPassFilterProcess,
#     'frequency domain homomorphic filter': FrequencyHomomorphicFilterProcess,
#     'frequency domain adaptive threshold': FrequencyDomainAdaptiveThresholdProcess,
#     'canny edge detection': CannyEdgeProcess,
#     'sobel edge detection': SobelEdgeProcess,
#     'laplacian edge detection': LaplacianEdgeProcess,
#     'geometric transform': GeoTransProcess,
#     'integer transform': IntTransProcess,
#     'crop': CropProcess,
#     'hough line transform': HoughLinesProcess,
#     'hough circle transform': HoughCirclesProcess,
#     'Harris corner detection': HarrisCornerDetectionProcess,
#     'Shi Thomas feature detection': ShiThomasDetectionProcess,
#     'SIFT feature detection': SIFTFeatureDetectionProcess,
#     'SURF feature detection': SURFFeatureDetectionProcess,
#     'FAST feature detection': FASTFeatureDetectionProcess,
#     'STAR+BRIEF feature detection': STARFeatureDetectionProcess,
#     'ORB feature detection': ORBFeatureDetectionProcess,
#     'match template': MatchProcess,
#     'brute force feature match': BfMatchProcess,
#     'FLANN kdtree feature match': FlannKdtreeMatchProcess,
#     'FLANN LSH feature match': FlannLshMatchProcess,
#     'find homography': HomographyProcess,
#     'detect': DetectProcess,
#     'recognize': RecognizeProcess,
#     'binary overlay': BinOverlayProcess,
#     'keypoint overlay': KpOverlayProcess,
#     'draw match': DrawMatchProcess,

#     '-- store': StoreMetaProcess,
#     '-- load': LoadMetaProcess,
#     '-- store and load': StoreLoadMetaProcess,
#     '-- comment': CommentMetaProcess,
# }

app_layouts = {
    'Tools left, images right': ToolsLeftImagesRightLayout(),
    'Tools, then images, horizontally': HorizontalLayout(),
    'Images, then tools, vertically': VerticalLayout(),
    'Tools vertically, then images, horizontally': HorizontalVerToolsLayout(),
    'Images, then tools horizontally, vertically': VerticalHorToolsLayout()
}

'''
Argparsing
'''

class MetavarOnceHelpFormatter(argparse.HelpFormatter):
    '''
    https://stackoverflow.com/a/31124505
    '''
    def __init__(self, prog):
        super().__init__(prog, max_help_position=40, width=80)
    def _format_action_invocation(self, action):
        if not action.option_strings or action.nargs == 0:
            return super()._format_action_invocation(action)
        default = self._get_default_metavar_for_optional(action)
        args_string = self._format_args(action, default)
        return ', '.join(action.option_strings) + ' ' + args_string

def parse_args():
    parser = argparse.ArgumentParser(
        description='Image processing utility.',
        formatter_class=MetavarOnceHelpFormatter
    )
    parser.add_argument('-i', '--image',
        dest='image',
        metavar='PATH',
        help='Input image to open. If channel is not specified, color image is loaded.'
    )
    parser.add_argument('-c', '--channel',
        dest='channel',
        choices=['R', 'G', 'B', 'Y', 'Cb', 'Cr', 'H', 'S', 'V', 'gray'],
        metavar='CHANNEL',
        help='Channel to be extracted from opened image. Image to be opened must also be provided.'
    )
    parser.add_argument('-p', '--procedure',
        dest='procedure',
        metavar='PATH',
        help='Procedure file to load.'
    )
    parser.add_argument('-t', '--template',
        dest='template',
        nargs=2,
        metavar=('NAME', 'PATH'),
        action='append',
        help='Template to load. May be specified multiple times. Type is determined from suffix.'
    )
    parser.add_argument('-L', '--layout',
        dest='layout',
        type=int,
        default=1,
        choices=list(range(1, 5+1)),
        metavar='INDEX',
        help='One-based index of the window layout to use, as listed in the app.'
    )
    parser.add_argument('-M', '--maximize',
        dest='maximize',
        action='store_true',
        help='Maximizes the application window on launch.'
    )
    args = parser.parse_args()
    if args.channel and not args.image:
        raise parser.error('Channel may only be specified if image to be loaded is provided.')
    return args

if __name__ == '__main__':
    args = parse_args()
    app = QtWidgets.QApplication(sys.argv)
    _ui = AppUi(args)
    _model = AppModel(args)
    _ctrl = AppControl(_ui, _model, args)
    _ui.show()
    sys.exit(app.exec_())
