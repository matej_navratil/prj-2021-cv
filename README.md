# Computer vision project

## en

### prog01.py

Usage:
```
prog01.py [-i input] -f func [-p params] [-o output]
```
The parameters `input` and `output`, if omitted, assume input and output on standard input and output, `stdin`/`stdout`, encoded as `ascii` and `base64`. The goal of this implementation is to allow for classic bashlike pipelines, as demonstrated in the example `findthree.sh`.

Currently, the following functions are implemented:
* `-f colorspace -p conversion_func` converts the image between two color spaces, for example `rgb2hsv`, `rgb2gray`, `bgr2rgb`, `bgr2hsv`.
* `-f inrange -p lower upper`, where `lower` and `upper` are vectors of dimension equal to the number of color channels of the used color space, keeps pixels whose all channels are in the range given by the two bounds. Other pixels are set to 0. Vector elements are delimited by commas with no spaces.
* `-f inrangeotherwise -p lower upper other` Functions like `inrange`, however pixels failing the range check are set to `other` instead.
* `-f thresh -p thresh_func thresh maxval` applies a global threshold to the image with values in range `0-maxval`. The image must be grayscale. Implements for example `binary` threshold func, which sets values above threshold to `maxval` and values below to 0.
* `-f adaptivethresh -p adaptive_func thresh_func maxval blocksize bias` applies a locally computed threshold to the image. `adaptive_func` may be `gaussian` or `mean`, denoting how the surrounding pixels will be weighted to compute the threshold. `blocksize` determines the size of the neighborhood used, and `bias` is subtracted from the threshold, shifting pixels towards a determined side of the threshold, should all nearby pixels be of similar color. The sudoku example `threshsudoku.sh` demonstrates this functionality.
* `-f gaussianblur -p size` applies Gaussian blur to the image with the provided kernel size. The example `threshthree.sh` demonstrates its use to erase anomalous noise that `findthree.sh` lets through.

## cz

### prog01.py

Použití:
```
prog01.py [-i input] -f func [-p params] [-o output]
```
Parametry `input` and `output`, pokud jsou vynechány, počítají se vstupem či výstupem na standardním vstupu či výstupu, a to v kódování `ascii` a `base64`. Úmyslem je umožnění typicky bashovského rourování, jak je vidět v příkladu `findthree.sh`.

Momentálně implementované funkce jsou `colorspace`, `inrange` a `inrangeotherwise`:
* `-f colorspace -p conversion_func` převede obrázek mezi dvěma barevnými prostory, například `rgb2hsv`, `rgb2gray`, `bgr2rgb`, `bgr2hsv`.
* `-f inrange -p lower upper`, kde `lower` a `upper` jsou vektory o rozměru odpovídajícímu počtu kanálů, zachová pixely, jejichž všechny kanály jsou v rozmezí. Ostatní jsou nastaveny na 0. Složky vektoru jsou odděleny čárkou bez mezery.
* `-f inrangeotherwise -p lower upper other` funguje podobně jako `inrange`, ale pixely neodpovídající podmínce jsou nastaveny na `other`.
* `-f thresh -p thresh_func thresh maxval` aplikuje globální prahovou funkci na obrázek o hodnotách v rozsahu `0-maxval`. Obrázek musí být v jednokanálovém formátu stupnice šedi. Implementuje například prahovou funkci `binary`, která nastaví pixely nad prahem na `maxval` a pixely pod prahem na 0.
* `-f adaptivethresh -p adaptive_func thresh_func maxval blocksize bias` na obrázek aplikuje lokálně spočtenou prahovou funkci. `adaptive_func` může být `gaussian` nebo `mean`, což rozhoduje, jaké váhy dostanou blízké pixely při výpočtu prahu. `blocksize` rozhoduje o velikosti použitého sousedství pixelu, a `bias` se od vypočteného prahu odečte, což posune pixely s relativně stejnobarevným sousestvím na jednu stranu prahu. Příklad použití je k vidění ve skriptu `threshsudoku.sh`.
* `-f gaussianblur -p size` na obrázek aplikuje Gaussovské rozorstření o zadané velikosti jádra. Příklad `threshthree.sh` ukazuje použití rozostření pro rozmazání hluku, který prošel příkladem `findthree.sh`.
