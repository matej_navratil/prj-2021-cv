#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=pointless-string-statement
"""
Created on Wed Feb 24 15:40:28 2021

@author: des
"""

import argparse
import base64
import cv2 as cv
import numpy as np

""" Definitions """

def image_in_func(input_arg):
    # cv.IMREAD_UNCHANGED to prevent 3-channelifying grayscale images
    if input_arg is not None:
        return cv.imread(input_arg, cv.IMREAD_UNCHANGED)
    im_string = input().encode('ascii')
    im_bytes = base64.b64decode(im_string)
    im_arr = np.frombuffer(im_bytes, dtype=np.uint8)
    return cv.imdecode(im_arr, cv.IMREAD_UNCHANGED)

def image_out_func(output_arg, image):
    if output_arg is not None:
        cv.imwrite(output_arg, image)
    else:
        im_arr = cv.imencode('.png', image)[1]
        im_bytes = im_arr.tobytes()
        im_string = base64.b64encode(im_bytes).decode('ascii')
        print(im_string)

class ProcessingAction:
    def __init__(self, func, nargs,
                 in_func = image_in_func, out_func = image_out_func):
        self.func = func
        self.nargs = nargs
        self.in_func = in_func
        self.out_func = out_func
    def __call__(self, subject, params):
        if params is None and self.nargs > 0:
            raise NargsException('ProcessingAction', self.nargs, 0)
        if len(params) == self.nargs:
            return self.func(subject, params)
        raise NargsException('ProcessingAction', self.nargs, len(params))
    def read(self, input_arg):
        return self.in_func(input_arg)
    def write(self, output_arg, subject):
        return self.out_func(output_arg, subject)

def processing_action_colorspace(subject, params):
    for (f_name, f_to_execute) in colorspace_conversion_funcs.items():
        if f_name.lower() == ("COLOR_" + params[0]).lower():
            return cv.cvtColor(subject, f_to_execute)
    raise ParamException('colorspace', 'conversion func', params[0])

def processing_action_inrange(subject, params):
    lower = np.array(list(map(np.uint8, params[0].split(','))))
    upper = np.array(list(map(np.uint8, params[1].split(','))))
    chans = subject.shape[2]
    if len(lower) != chans:
        raise ChannelCountException('inrange lower', chans, len(lower))
    if len(upper) != chans:
        raise ChannelCountException('inrange upper', chans, len(upper))
    mask = cv.inRange(subject, lower, upper)
    return cv.bitwise_and(subject, subject, mask=mask)

def processing_action_inrangeotherwise(subject, params):
    lower = np.array(list(map(np.uint8, params[0].split(','))))
    upper = np.array(list(map(np.uint8, params[1].split(','))))
    other = np.array(list(map(np.uint8, params[2].split(','))))
    chans = subject.shape[2]
    if len(lower) != chans:
        raise ChannelCountException('inrange lower', chans, len(lower))
    if len(upper) != chans:
        raise ChannelCountException('inrange upper', chans, len(upper))
    if len(other) != chans:
        raise ChannelCountException('inrange other', chans, len(other))
    mask = cv.inRange(subject, lower, upper)
    result_img = cv.bitwise_and(subject, subject, mask=mask)
    mask = cv.bitwise_not(mask)
    result_img[mask] = other
    return result_img

def processing_action_thresh(subject, params):
    if len(subject.shape) > 3 and subject.shape[2] != 1:
        raise ImageChannelCountException('thresh', 1, subject.shape[2])
    thresh = int(params[1])
    maxval = int(params[2])
    for (f_name, f_to_execute) in thresh_funcs.items():
        if f_name.lower() == ('THRESH_' + params[0]).lower():
            return cv.threshold(subject, thresh, maxval, f_to_execute)[1]
    raise ParamException('thresh', 'thresholding func', params[0])

def processing_action_adaptivethresh(subject, params):
    if len(subject.shape) > 3 and subject.shape[2] != 1:
        raise ImageChannelCountException('adaptive thresh', 1, subject.shape[2])
    maxval = int(params[2])
    block_size = int(params[3])
    bias = int(params[4])
    f_adaptive = None
    f_threshold = None
    for (f_name, f_adaptive) in adaptive_thresh_funcs.items():
        if f_name.lower() == ('ADAPTIVE_THRESH_' + params[0] + '_C').lower():
            break
    else:
        raise ParamException('adaptive thresh', 'adaptive func', params[0])
    for (f_name, f_threshold) in thresh_funcs.items():
        if f_name.lower() == ('THRESH_' + params[1]).lower():
            break
    else:
        raise ParamException('adaptive thresh', 'thresholding func', params[1])
    return cv.adaptiveThreshold(subject, maxval, f_adaptive,
                                f_threshold, block_size, bias)

def processing_action_gaussianblur(subject, params):
    kernel_size = int(params[0])
    return cv.GaussianBlur(subject, (kernel_size, kernel_size), 0)

class NargsException(Exception):
    def __init__(self, ctx: str, expected: int, given: int):
        super().__init__(('Wrong number of params in context %s: '
                          '%d expected, %d given') % (ctx, expected, given))
        self.expected = expected
        self.given = given

class ParamException(Exception):
    def __init__(self, ctx: str, expected: str, given: str):
        super().__init__(('Unknown param in context %s: '
                          '%s expected, %s given') % (ctx, expected, given))
        self.expected = expected
        self.given = given

class ChannelCountException(Exception):
    def __init__(self, ctx: str, expected: int, given: int):
        super().__init__(('Wrong channel count in context %s: Image has '
                          '%d channels, %d given') % (ctx, expected, given))
        self.expected = expected
        self.given = given

class ImageChannelCountException(Exception):
    def __init__(self, ctx: str, expected: int, given: int):
        super().__init__(('Wrong channel count in context %s: Image has '
                          '%d channels, %d expected') % (ctx, given, expected))
        self.expected = expected
        self.given = given

""" Initialization """

colorspace_conversion_funcs = {
    i: vars(cv)[i] for i in dir(cv) if i.startswith('COLOR_')
}

thresh_funcs = {
    i: vars(cv)[i] for i in dir(cv) if i.startswith('THRESH_')
}

adaptive_thresh_funcs = {
    i: vars(cv)[i] for i in dir(cv) if i.startswith('ADAPTIVE_THRESH_')
}

processing_funcs = {
    'colorspace': ProcessingAction(processing_action_colorspace, 1),
    'inrange': ProcessingAction(processing_action_inrange, 2),
    'inrangeotherwise': ProcessingAction(processing_action_inrangeotherwise, 3),
    'thresh': ProcessingAction(processing_action_thresh, 3),
    'adaptivethresh': ProcessingAction(processing_action_adaptivethresh, 5),
    'gaussianblur': ProcessingAction(processing_action_gaussianblur, 1),
}

""" CLI initialization """

parser = argparse.ArgumentParser(description='Process an image.')

parser.add_argument('-i', '--input', metavar='FILE', type=str, nargs=1,
                    dest='input', #action='store_const',
                    help='The file, usually image, to input from. ' +
                    'If omitted, stdin will be assumed for pipelining.')

parser.add_argument('-o', '--output', metavar='FILE', type=str, nargs=1,
                    dest='output', #action='store_const',
                    help='The file, usually image, to output to. ' +
                    'If omitted, stdout will be assumed for pipelining.')

parser.add_argument('-f', '--function', metavar='FUNC', type=str, nargs=1,
                    dest='function', #action='store_const',
                    choices=processing_funcs.keys(),
                    help='The function to apply to the input.')

parser.add_argument('-p', '--params', metavar='PARAMS', type=str, nargs='+',
                    dest='params', #action='store_const',
                    help='Parameters of the function.')

""" Live code """

args = parser.parse_args()
# Read -f, which must be specified, and -p, which may be empty.
to_apply = processing_funcs[args.function[0]]
in_params = args.params
# Read -i, or None if unspecified, and read input, typically an image.
input_subject = to_apply.read(args.input[0] if args.input else None)
# Process the subject, typically an image, according to -f and -p.
output_subject = to_apply(input_subject, in_params)
# Read -o, or None if unspecified, and write output, typically an image.
to_apply.write(args.output[0] if args.output else None, output_subject)
